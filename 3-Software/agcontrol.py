
#--------------------------------------------------------------------------------
# Initial imports
#--------------------------------------------------------------------------------
import serial  	# library for serial communication
import binascii	# hexadecimal to binary conversion
import sys		# for sys.argv
import os		# for chmod
from datetime import datetime
from fcts import printf,from_bytes,to_bytes, json_setup
from subprocess import *
import time
import threading,thread
import json
import Tkinter as tk
import serial.tools.list_ports
import math
from decimal import Decimal
import copy
from Converter import Converter
from Ownwallgui import Ownwallgui
				
				
#initializang variables and configurations

port=[]
ownwall=[]


#definyng mode, Vref and getting the port names for every ownwall connected either by user entry either by hardware check #verify on linux
if len(sys.argv)>1:
    Converter.mode=sys.argv[1]
if len(sys.argv)>2:
    Converter.Vref=int(sys.argv[2])
if len(sys.argv)>3:
    for i in range(len(sys.argv)-3):
        port.append(sys.argv[i+3])
    # for i in range(len(port)):
    #     print port[i]
else:
    for device in serial.tools.list_ports.comports():
		if device.name != 'ttyAMA0':
			port.append(device.name)
	        print "lista de entradas seriais ", device.name


for portname in port:
    ownwall.append(Converter(portname))
    print 'ownwall da porta'+portname+'passou'


while not ownwall[0].newmeasureflag:
	time.sleep(0.0001)
	pass

gui=Ownwallgui(ownwall)
S1=0
E1=0
p_ratio1=[]
listlenght=0

print 'voltage control loop start'
while 1:
	
	time.sleep(0.00001)		
#~ waiting and saving new measures
	vL=[]
	vH=[]
	T=[]
	p_ratio=[]
	tm=0.0
	ownwallvloop=[]
	for device in ownwall:
		if device.voltage_control:
			ownwallvloop.append(device)
	if len(ownwallvloop) != listlenght:
		listlenght=len(ownwallvloop)
		p_ratio1=[]
		if listlenght>0:
			for i in range(listlenght): p_ratio1.append(round(1.0/listlenght,4))
		
	if listlenght>0:		
		for device in ownwallvloop:
			while not device.newmeasureflag:
				time.sleep(0.00001)
				pass
			vL.append(device.measures[1])
			vH.append(device.measures[5])
			T.append(device.measures[3])
			device.newmeasureflag=0
					
		#~ calculating the reference power through the discrete PI
		#~ S=r0*E+r1*E(-1)+S(-1)
		E=Converter.Vref-vH[0]
		S=Converter.r0*E+Converter.r1*E1+S1 #pi operation, S=iRef for one converter
		E1=E
		S1=S
		Pref=S*vL[0]
		
				
		#~ calculating the power proportions for each converter based on their temperatures
		if Converter.tempctrl:
			for i in range(listlenght): tm+=T[i]
			tm/=listlenght
			for i in range(listlenght):
				p_ratio.append(p_ratio1[i]+Converter.ktempctrl*(tm-T[i]))
			
			#~ defining boundaries
			if sum(p_ratio)>1.00001			:
				p_ratio=copy.deepcopy(p_ratio1)
			for i in range(listlenght): 
				if p_ratio[i]<0.05 :
					p_ratio=copy.deepcopy(p_ratio1)
					break
			p_ratio1=copy.deepcopy(p_ratio)
			
		#power standard division
		else:
			for i in range(listlenght): p_ratio.append(round(1.0/listlenght,4))
			p_ratio1=copy.deepcopy(p_ratio)
			
			
		#calculation of iref, conversion to raw iref and sending the command to the firmware
		for i in range(listlenght):
			iref=-Pref*p_ratio[i]/vL[i]
			x=iref*ownwallvloop[i].cha_cal[2]+ownwallvloop[i].cha_offset[2]
			if x>=4095 : iref_raw=4095
			elif x<=1 : iref_raw=1
			else: iref_raw=x
			while ownwallvloop[i].sendingcmd:
				time.sleep(0.00001)
				pass
			ownwallvloop[i].sendcmd('setting',iref_raw)
			
	
