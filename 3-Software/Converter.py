#--------------------------------------------------------------------------------
# Initial imports
#--------------------------------------------------------------------------------
import serial  	# library for serial communication
from datetime import datetime
from fcts import printf,from_bytes,to_bytes, json_setup
import time
import threading,thread
import json
import math
from decimal import Decimal
import copy

class Converter:
	
	mode="BOOST"
	tempctrl=1
	ktempctrl=0.001
	Cs=440.0E-6;
	Ls=100.0E-3;
	R=100.0;
	Vb=12.0;
	Rb=20.0E-3;
	Te=0.04
	trdes=0.4
	Vref=48.0
	r1=0.0215
	r0=-0.0078
	active_converters=0
	cha_name = ["Pgood", "VmL",	"VmiL",	"VmT1",	"VmiH",	"VmH", "PWM"]
	cha_unit =   [	  "raw",   "V",	   "A",	   "oC",  "A",	  "V",	 "%"]

	def __init__(self, port, quiet=1):
		
		#~ j=json.loads(jsonsetup)
		#~ self.board_id=j['board_id']
		#~ self.duty_cycle=j['duty_cycle']
		#~ self.mesure_cha=j['mesure_cha']
		#~ self.setting=j['setting']
		#~ self.tracking_on=j['tracking_on']
		#~ self.pwm_run=j['pwn_run']
		#~ self.vmil_cal=j['vmil_cal']
		#~ self.vmil_offset=j['vmil_offset']
		#~ self.vmih_cal=j['vmih_cal']
		#~ self.vmih_offset=j['vmih_offset']
		#~ self.vml_cal=j['vml_cal']
		#~ self.vmh_cal=j['vmh_cal']
		#~ self.vmt1_cal=j['vmt1_cal']
		#~ self.Pgood='1'
		#~ self.VmL='1'
		#~ self.VmiL='1'
		#~ self.VmT1='1'
		#~ self.VmiH='1'
		#~ self.VmH='1'
		#~ self.PWM='1'
		
		self.voltage_control=0
		self.port=port		
		self.quiet = quiet   # defines if the port is on quiet mode
		self.newmeasureflag=0
		self.newdisplaymeasureflag=0
		self.sendingcmd=0
		self.measures= [0.0,0.0,0.0,0.0,0.0,0.0,0.0]
		self.cha_cal = 	 [	      1,	 1,	     1,	     1,	     1,	    1,	   1]
		self.cha_offset = [	      0,     0,	  2068,		 0,	  2068,		0,     0]
		
		self.serialinit()
		threadgetsetup=threading.Thread(target=self.getsetup,args=())
		threadgetsetup.start()
		
				
		Converter.active_converters+=1
	
	def serialinit(self):
		# initializing serial communication (this will reset the nano)
		self.ser0 = serial.Serial(
			port="/dev/"+self.port,
			baudrate=57600,
			bytesize=serial.EIGHTBITS,
			parity=serial.PARITY_NONE,
			stopbits=serial.STOPBITS_ONE,
			timeout=None)

	def getsetup(self):
		# reading board setup from serial
		self.ser0.write('p')
		self.board_id = from_bytes(self.ser0.read())
		
		duty_cycle_high_byte = from_bytes(self.ser0.read())
		duty_cycle_low_byte = from_bytes(self.ser0.read())
		
		self.measure_cha = from_bytes(self.ser0.read())
		
		setting_high_byte = from_bytes(self.ser0.read())
		setting_low_byte = from_bytes(self.ser0.read())
		
		self.tracking_on = from_bytes(self.ser0.read())
		
		self.pwm_run = from_bytes(self.ser0.read())
		
		vmil_cal_high_byte = from_bytes(self.ser0.read())
		vmil_cal_low_byte = from_bytes(self.ser0.read())
		
		vmil_offset_high_byte = from_bytes(self.ser0.read())
		vmil_offset_low_byte = from_bytes(self.ser0.read())
		
		vmih_cal_high_byte = from_bytes(self.ser0.read())
		vmih_cal_low_byte = from_bytes(self.ser0.read())
		
		vmih_offset_high_byte = from_bytes(self.ser0.read())
		vmih_offset_low_byte = from_bytes(self.ser0.read())
		
		vml_cal_high_byte = from_bytes(self.ser0.read())
		vml_cal_low_byte = from_bytes(self.ser0.read())
		
		vmh_cal_high_byte = from_bytes(self.ser0.read())
		vmh_cal_low_byte = from_bytes(self.ser0.read())
		
		vmt1_cal_high_byte = from_bytes(self.ser0.read())
		vmt1_cal_low_byte = from_bytes(self.ser0.read())

		# computing words
		self.duty_cycle = duty_cycle_high_byte*256 + duty_cycle_low_byte
		
		self.setting = setting_high_byte*256 + setting_low_byte
		
		self.vmil_cal = vmil_cal_high_byte*256 + vmil_cal_low_byte
		
		self.vmil_offset = vmil_offset_high_byte*256 + vmil_offset_low_byte
		
		self.vmih_cal = vmih_cal_high_byte*256 + vmih_cal_low_byte
		
		self.vmih_offset = vmih_offset_high_byte*256 + vmih_offset_low_byte
		
		self.vml_cal = vml_cal_high_byte*256 + vml_cal_low_byte
		
		self.vmh_cal = vmh_cal_high_byte*256 + vmh_cal_low_byte
		
		self.vmt1_cal = vmt1_cal_high_byte*256 + vmt1_cal_low_byte
		
		# Setting calibers and offsets (V4.1)
		#~ cha_cal[0] = vmil_cal/100.0;
		#~ cha_offset[0] = vmil_offset;
		#~ cha_cal[1] = vmih_cal/100.0;
		#~ cha_offset[1] = vmih_offset;
		#~ cha_cal[2] = vml_cal/100.0;
		#~ cha_cal[3] = vmh_cal/100.0;
		#~ cha_cal[4] = vmt1_cal/100.0;
		
		# Setting calibers and offsets (V4.2)
		self.cha_cal[Converter.cha_name.index("VmiL")]    = self.vmil_cal/100.0;
		self.cha_offset[Converter.cha_name.index("VmiL")] = self.vmil_offset;
		self.cha_cal[Converter.cha_name.index("VmiH")]    = self.vmih_cal/100.0;
		self.cha_offset[Converter.cha_name.index("VmiH")] = self.vmih_offset;
		self.cha_cal[Converter.cha_name.index("VmL")]     = self.vml_cal/100.0;
		self.cha_cal[Converter.cha_name.index("VmH")]     = self.vmh_cal/100.0;
		self.cha_cal[Converter.cha_name.index("VmT1")]    = self.vmt1_cal/100.0;
		
		# If caliber==0 default to 1
		for i in range(0,5):
			if self.cha_cal[i]==0: self.cha_cal[i]=1;
		
		# printing setup to stdout
		if not self.quiet:
			print """
			***************
			* Board setup *
			***************
			"""
			
			print "Board ID: "+str(self.board_id)
			
			print "Duty-cycle = "+str(self.duty_cycle)
			
			print "Tracking channel is",
			if self.measure_cha < len(self.cha_name): print self.cha_name[self.measure_cha]
			else: print "not defined"
			
			print "Tracked value = "+str(self.setting)
			
			print "Tracking is",
			if self.tracking_on: print "on"
			else: print "off"
			
			print "PWM state is",
			if self.pwm_run: print "running"
			else: print "stopped"
			
			print "vmil_cal = "+str(self.vmil_cal)
			print "vmil_offset = "+str(self.vmil_offset)
			print "vmih_cal = "+str(self.vmih_cal)
			print "vmih_offset = "+str(self.vmih_offset)
			print "vml_cal = "+str(self.vml_cal)
			print "vmh_cal = "+str(self.vmh_cal)
			print "vmt1_cal = "+str(self.vmt1_cal)
			
			print
		
		# writing json setup file
		json_setup(self.port, self.board_id, self.duty_cycle, self.measure_cha, self.setting,
		self.tracking_on, self.pwm_run, self.vmil_cal, self.vmil_offset, self.vmih_cal, self.vmih_offset,
		self.vml_cal, self.vmh_cal, self.vmt1_cal)
		
		self.createoutputfile()

	def createoutputfile(self):
		# writing output file header
		capture_date = str(datetime.now())[0:19].replace(' ','_').replace('-','').replace(':','')
		self.time = .0
		self.out = open('/home/pi/Desktop/Ownwall/capture_'+str(self.board_id)+'_'+capture_date+'.out','w')
		outstr= "#Time"
		for i in range(0,7):
			outstr += " "+self.cha_name[i]
		self.out.write(outstr+"\n"+"0 0 0 0 0 0 0 0\n")
		
		self.measuresthread=threading.Thread(target=self.getmeasures,args=())
		self.measuresthread.start()
		

	def getmeasures(self):
		int_measure      = [0,0,0,0,0,0,0]
		int_raw_mes     = [0,0,0,0,0,0,0]
		int_avg_mes     = [0,0,0,0,0,0,0]
		int_raw_avg_mes = [0,0,0,0,0,0,0]
		self.displaymeasures=[0,0,0,0,0,0,0]
		avg_counter     = 0
		self.avg_number      = 10 # number of measures to average
		cmd             = ""
		self.ser0.write('p')
		print 'enviado'
		while 1:
			# reading 7 measures from serial + 2 dummy bytes
			i=0
			for byte in self.ser0.read(16):
				if i<14:
					if i%2==0: 	# first we read bits[15:8]
						high_byte = from_bytes(byte, byteorder='big')
					else:		# then we read bits[7:0]
						low_byte = from_bytes(byte, byteorder='big')
								# we make a word from this two bytes and we calulate the float value
						int_raw_mes[i/2] = high_byte*256 + low_byte
						int_raw_avg_mes[i/2] += int_raw_mes[i/2]
						int_measure[i/2] = float((int_raw_mes[i/2] - self.cha_offset[i/2])/self.cha_cal[i/2])
						int_avg_mes[i/2] += int_measure[i/2]
				i += 1
		
			# displaying measures in terminal and writing it to json measures file
			str_mes = "\r"
			avg_counter += 1
			if avg_counter == self.avg_number:
				
				self.newmeasureflag=0
				self.json_mes = "{"
				avg_counter = 0
				for j in range(0,7):
					int_avg_mes[j] /= self.avg_number
					int_raw_avg_mes[j] /= self.avg_number
					
					
					spaces = ""
					#if int_avg_mes[j] < 1000: spaces += " " # will always occure
					if int_avg_mes[j] < 100: spaces += " "
					if int_avg_mes[j] < 10: spaces += " "
					if int_avg_mes[j] >=0: spaces += " "
					if int_raw_avg_mes[j] < 1000: spaces += " "
					if int_raw_avg_mes[j] < 100: spaces += " "
					if int_raw_avg_mes[j] < 10: spaces += " "
					str_mes += "%s=%.2f%s (%d) "+spaces
					self.json_mes += '"'+self.cha_name[j]+'":'+str(int(int_avg_mes[j]*100)/100.0)
					if j<6: self.json_mes += ','
				self.json_mes += '}'
				json = open('/home/pi/Desktop/Ownwall/'+self.port+'_measures.json', 'w')
				json.write(self.json_mes)
				json.close()
				# now we print it
				if not self.quiet:
					printf(str_mes+cmd,
						self.cha_name[0], int_avg_mes[0], self.cha_unit[0], int_raw_avg_mes[0],
						self.cha_name[1], int_avg_mes[1], self.cha_unit[1], int_raw_avg_mes[1],
						self.cha_name[2], int_avg_mes[2], self.cha_unit[2], int_raw_avg_mes[2],
						self.cha_name[3], int_avg_mes[3], self.cha_unit[3], int_raw_avg_mes[3],
						self.cha_name[4], int_avg_mes[4], self.cha_unit[4], int_raw_avg_mes[4],
						self.cha_name[5], int_avg_mes[5], self.cha_unit[5], int_raw_avg_mes[5],
						self.cha_name[6], int_avg_mes[6], self.cha_unit[6], int_raw_avg_mes[6]
					)
					
				self.measures=copy.deepcopy(int_avg_mes)
				self.displaymeasures=self.measures
				#~ self.displaymeasures[2]=copy.deepcopy(int_raw_avg_mes[2])
								
				for j in range(0,7):
					int_avg_mes[j] = 0
					int_raw_avg_mes[j] = 0
					
				self.newdisplaymeasureflag=1							
				self.newmeasureflag=1
				
		
			# Writing measures to capture file
			self.time += .00384
			self.out.write(str(self.time)+" "+str(int_measure[0])+" "+str(int_measure[1])
			+" "+str(int_measure[2])+" "+str(int_measure[3])+" "
			+str(int_measure[4])+" "+str(int_measure[5])+" "
			+str(int_measure[6])+"\n")
			
		
	def sendcmd(self,variable, value):
		
		self.sendingcmd=1
		# set uint8_t board_id
		if(variable=='board_id'):
			self.board_id = int(value)
			self.ser0.write('b')
			self.ser0.write(to_bytes(self.board_id, 1))
		
		# set uint16_t duty_cycle
		elif(variable=='duty_cycle'):
			self.duty_cycle = int(value)
			dc_high_byte = to_bytes(int(value)>>8, 1)
			dc_low_byte = to_bytes(int(value)&0x00ff, 1)
			self.ser0.write('d')
			self.ser0.write(dc_high_byte)
			self.ser0.write(dc_low_byte)
		
		# set uint8_t measure_cha
		elif(variable=='measure_cha'):
			self.measure_cha = int(value)
			self.ser0.write('m')
			self.ser0.write(to_bytes(int(value), 1))
		
		# set uint16_t setting
		elif(variable=='setting'):
			self.setting = int(value)
			s_high_byte = to_bytes(int(value)>>8, 1)
			s_low_byte = to_bytes(int(value)&0x00ff, 1)
			self.ser0.write('s')
			self.ser0.write(s_high_byte)
			self.ser0.write(s_low_byte)
		
		# set uint8_t tracking_on
		elif(variable=='tracking_on'):
			self.tracking_on = int(value)
			self.ser0.write('t')
			self.ser0.write(to_bytes(int(value), 1))
		
		# set uint8_t pwm_run
		elif(variable=='pwm_run'):
			self.pwm_run = int(value)
			self.ser0.write('p')
			self.ser0.write(to_bytes(int(value), 1))
		
		# set uint16_t vmil_cal
		elif(variable=='vmil_cal'):
			self.vmil_cal = int(value)
			self.cha_cal[self.cha_name.index("VmiL")] = self.vmil_cal/100.0;
			e_high_byte = to_bytes(int(value)>>8, 1)
			e_low_byte = to_bytes(int(value)&0x00ff, 1)
			self.ser0.write('e')
			self.ser0.write(e_high_byte)
			self.ser0.write(e_low_byte)
		
		# set uint16_t vmil_offset
		elif(variable=='vmil_offset'):
			self.vmil_offset = int(value)
			self.cha_offset[self.cha_name.index("VmiL")] = self.vmil_offset;
			f_high_byte = to_bytes(int(value)>>8, 1)
			f_low_byte = to_bytes(int(value)&0x00ff, 1)
			self.ser0.write('f')
			self.ser0.write(f_high_byte)
			self.ser0.write(f_low_byte)
		
		# set uint16_t vmih_cal
		elif(variable=='vmih_cal'):
			self.vmih_cal = int(value)
			self.cha_cal[self.cha_name.index("VmiH")] = self.vmih_cal/100.0;
			g_high_byte = to_bytes(int(value)>>8, 1)
			g_low_byte = to_bytes(int(value)&0x00ff, 1)
			self.ser0.write('g')
			self.ser0.write(g_high_byte)
			self.ser0.write(g_low_byte)
		
		# set uint16_t vmih_offset
		elif(variable=='vmih_offset'):
			self.vmih_offset = int(value)
			self.cha_offset[self.cha_name.index("VmiH")] = self.vmih_offset;
			h_high_byte = to_bytes(int(value)>>8, 1)
			h_low_byte = to_bytes(int(value)&0x00ff, 1)
			self.ser0.write('h')
			self.ser0.write(h_high_byte)
			self.ser0.write(h_low_byte)
		
		# set uint16_t vml_cal
		elif(variable=='vml_cal'):
			self.vml_cal = int(value)
			self.cha_cal[self.cha_name.index("VmL")] = self.vml_cal/100.0;
			i_high_byte = to_bytes(int(value)>>8, 1)
			i_low_byte = to_bytes(int(value)&0x00ff, 1)
			self.ser0.write('i')
			self.ser0.write(i_high_byte)
			self.ser0.write(i_low_byte)
		
		# set uint16_t vmh_cal
		elif(variable=='vmh_cal'):
			self.vmh_cal = int(value)
			self.cha_cal[self.cha_name.index("VmH")] = self.vmh_cal/100.0;
			j_high_byte = to_bytes(int(value)>>8, 1)
			j_low_byte = to_bytes(int(value)&0x00ff, 1)
			self.ser0.write('j')
			self.ser0.write(j_high_byte)
			self.ser0.write(j_low_byte)
		
		# set uint16_t vmt1_cal
		elif(variable=='vmt1_cal'):
			self.vmt1_cal = int(value)
			self.cha_cal[self.cha_name.index("VmT1")] = self.vmt1_cal/100.0;
			k_high_byte = to_bytes(int(value)>>8, 1)
			k_low_byte = to_bytes(int(value)&0x00ff, 1)
			self.ser0.write('k')
			self.ser0.write(k_high_byte)
			self.ser0.write(k_low_byte)
		
		# writing json setup file
		json_setup(self.port, self.board_id, self.duty_cycle, self.measure_cha, self.setting,
		self.tracking_on, self.pwm_run, self.vmil_cal, self.vmil_offset, self.vmih_cal, self.vmih_offset,
		self.vml_cal, self.vmh_cal, self.vmt1_cal)
		
		self.sendingcmd=0

	@classmethod
	def setnewpi(cls):
		a=1.0
		b=-cls.Vb
		c=cls.Rb*(cls.Voref**2)/cls.R
		delta = (b**2) - (4*a*c)
		solution1 = (-b-math.sqrt(delta))/(2*a)
		solution2 = (-b+math.sqrt(delta))/(2*a)
		
		if solution1>solution2:
			Vc1e=solution1
		else:
			Vc1e=solution2
		alphae=Vc1e/Voref
		Ile=cls.Voref/(cls.R*alphae)
		    
		Kv=alphae*cls.R*Voref/(Voref+cls.R*Ile*alphae)
		Tv=cls.Cs*cls.R*cls.Voref/(cls.Voref+cls.R*Ile*alphae)
		
		b1=Kv*(1-math.exp(-cls.Te/Tv))
		a1=-math.exp(-cls.Te/Tv)
		
		z=math.exp(-cls.Te*4.75/cls.trdes)
		p1=-2*z
		p2=z**2
		cls.r1=(p2+a1)/b1
		cls.r0=(p1-a1+1)/b1
	
