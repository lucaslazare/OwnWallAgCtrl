#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  
#  fcts.py
#
#  Copyright 2018 hugues <hugues.larrive@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

from __future__ import print_function
def printf(str, *args):
    print(str % args, end='')

import struct
import binascii

def from_bytes(b, byteorder='big'):
	# type: (bytes, str) -> Int
	val = bytes(memoryview(b).tobytes())
	byteorder = byteorder.lower()
	ch = '>' if byteorder.startswith('b') else '<'
	hi = struct.unpack(ch + 'B'*len(val), val)
	xo = 0 + sum(n**(i+1) for i, n in enumerate(hi))
	return xo

def to_bytes(integer, length, byteorder='big'):
	try:
		ch = '>' if byteorder.startswith('b') else '<'
		hex_str = '%x' % int(integer)
		n = len(hex_str)
		x = binascii.unhexlify(hex_str.zfill(n + (n & 1)))
		val = x[-1 * length:] if ch == '>' else x[:length]
	except OverflowError:
		raise ValueError(integer)
	return val

#--------------------------------------------------------------------------------
# FUNCTION json_setup
# DESCRIPTION - Writes a JSON file with all the configurations of the power converter
#--------------------------------------------------------------------------------

def json_setup(port, board_id, duty_cycle, measure_cha, setting, tracking_on, pwm_run, vmil_cal, vmil_offset, vmih_cal, vmih_offset, vml_cal, vmh_cal, vmt1_cal):
	setup = open('/home/pi/Desktop/Ownwall/'+port+'_setup.json', 'w')
	setup.write('{')
	#setup data
	setup.write('"board_id":'+str(board_id)+',')
	setup.write('"duty_cycle":'+str(duty_cycle)+',')
	setup.write('"measure_cha":'+str(measure_cha)+',')
	setup.write('"setting":'+str(setting)+',')
	setup.write('"tracking_on":'+str(tracking_on)+',')
	setup.write('"pwm_run":'+str(pwm_run)+',')
	#measurement-related data
	setup.write('"vmil_cal":'+str(vmil_cal)+',')
	setup.write('"vmil_offset":'+str(vmil_offset)+',')
	setup.write('"vmih_cal":'+str(vmih_cal)+',')
	setup.write('"vmih_offset":'+str(vmih_offset)+',')
	setup.write('"vml_cal":'+str(vml_cal)+',')
	setup.write('"vmh_cal":'+str(vmh_cal)+',')
	setup.write('"vmt1_cal":'+str(vmt1_cal))
	setup.write('}')
	setup.close()
