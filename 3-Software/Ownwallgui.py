
#--------------------------------------------------------------------------------
# Initial imports
#--------------------------------------------------------------------------------
import serial  	# library for serial communication
import binascii	# hexadecimal to binary conversion
import sys		# for sys.argv
import os		# for chmod
from datetime import datetime
from fcts import printf,from_bytes,to_bytes, json_setup
from subprocess import *
import time
import threading,thread
import json
import Tkinter as tk
import serial.tools.list_ports
import math
from decimal import Decimal
import copy
from Converter import Converter

	
class Ownwallgui(threading.Thread):
	
	def __init__(self,ownwall):
		self.ownwall=ownwall
		threading.Thread.__init__(self)
		self.start()
		
		
	def run(self):
		
		self.mpage=tk.Tk()
		self.devicewindows=[]
		self.mpage.title('OwnWall GUI')
		self.mpage.resizable(width=False,height=False)
		
		x=self.mpage.winfo_screenwidth()
		y=self.mpage.winfo_screenheight()
		self.mpage.geometry('+'+str(x/2-x/10)+'+'+str(y/2-y/6))
		
		self.mpframe=tk.Frame(self.mpage,bg='black')
		self.mpframe.pack(fill='both',expand=True)
		self.mpframe.grid_rowconfigure(0, weight=1)
		self.mpframe.grid_columnconfigure(0, weight=1)
		self.mframestart()
		
		self.mpage.mainloop()
		
		
	def mframestart(self):
		self.frameconfig=tk.LabelFrame(self.mpframe,text='Configurations',labelanchor='n',font='TimesNewRoman 18',fg='white',bg='black',bd=5,pady=1,relief='raised')
		self.framedevices=tk.LabelFrame(self.mpframe,text='OwnWall Converters',labelanchor='n',font='TimesNewRoman 18',fg='white',bg='black',bd=5,pady=1,padx=5,relief='raised')
		self.frameconfig.pack(fill='y',pady=15)
		self.framedevices.pack(expand=True,anchor='n',pady=1)
		
		#~ self.fconfleft=tk.Frame(self.frameconfig,bg='grey',bd=5)
		#~ self.fconfleft.grid(row=0,column=0)
		#~ self.fdevleft.pack(side='left',expand=True,fill='both')
		#~ self.fconfright=tk.Frame(self.frameconfig,bg='red',bd=5)
		#~ self.fdevright.pack(side='right',expand=True,fill='both')
		#~ self.fconfright.grid(row=0,column=1)
		
		#~ self.modelabel=tk.Label(self.fconfleft,anchor='e',text='Mode',bg='black',fg='white',font='TimesNewRoman 14',pady=3)
		#~ self.modelabel.pack(fill='both',pady=3)
		#~ self.vreflabel=tk.Label(self.fconfleft,anchor='e',text='Vref',bg='black',fg='white',font='TimesNewRoman 14',pady=3)
		#~ self.vreflabel.pack(fill='both',pady=3)
		#~ self.tctrllabel=tk.Label(self.fconfleft,anchor='e',text='Temperature Control',bg='black',fg='white',font='TimesNewRoman 14',pady=3)
		#~ self.tctrllabel.pack(fill='both',pady=3)
		
		#~ self.modevar = tk.StringVar(self.fconfright)
		#~ self.modevar.set("BOOST") # default value
		#~ self.modemenu=tk.OptionMenu(self.fconfright, self.modevar, 'BOOST','BUCK')
		#~ self.modemenu.pack(fill='both',pady=3)
		#~ self.ventry=tk.Entry(self.fconfright)
		#~ self.ventry.pack(fill='both',pady=3)
		
	
		self.modelabel=tk.Label(self.frameconfig,anchor='e',text='Mode',bg='black',fg='white',font='TimesNewRoman 14',pady=3)
		self.modelabel.grid(row=0,column=0,sticky='e',padx=5)
		self.vreflabel=tk.Label(self.frameconfig,anchor='e',text='Vref',bg='black',fg='white',font='TimesNewRoman 14',pady=3)
		self.vreflabel.grid(row=1,column=0,sticky='e',padx=5)
		self.tctrllabel=tk.Label(self.frameconfig,anchor='e',text='Temp. Control',bg='black',fg='white',font='TimesNewRoman 14',pady=3)
		self.tctrllabel.grid(row=2,column=0,sticky='e',padx=5)
		
		self.modevar = tk.StringVar(self.frameconfig)
		self.modevar.set(Converter.mode) # default value
		self.modemenu=tk.OptionMenu(self.frameconfig, self.modevar, 'BOOST','BUCK')
		self.modemenu.configure(bg='black',fg='white',activebackground='black',activeforeground='white',indicatoron=0,bd=0,pady=2,padx=0,width=10)
		self.modemenu['menu'].configure(bg='black',fg='white',borderwidth=6,relief='flat')
		self.modemenu.grid(row=0,column=1)
		self.modevar.trace('w',lambda *args: self.updatemode())
		
		self.ventry=tk.Entry(self.frameconfig,justify='center',width=10,bg='black',fg='white',insertbackground='white',highlightbackground='black')
		self.ventry.grid(row=1,column=1)
		self.ventry.insert(0,Converter.Vref)
		self.ventry.bind('<Return>',self.updatevref)
		
		self.tempb=tk.Button(self.frameconfig,bd=0,bg='black',pady=0,fg='white',font='Arial 10',width=10,padx=5,command=self.updatetempcontrol)
		if Converter.tempctrl:
			self.tempb['text']='ON'
		else:
			self.tempb['text']='OFF'	
		self.tempb.grid(row=2,column=1)
		
		self.devicebuttons=[]	
		for i in range(len(self.ownwall)):
			self.devicebuttons.append(tk.Button(self.framedevices,text=self.ownwall[i].port,height=2,font='Arial 12',bd=0,bg='black',pady=0,fg='white',width=10,padx=5))
			self.devicebuttons[i].bind('<Button-1>', lambda event:threading.Thread(target=self.createnewwindowdevice,args=(event,)).start())
			self.devicebuttons[i].pack(fill='both',expand=True,pady=2)
				

	def updatemode(self):
		Converter.mode=self.modevar.get()
		#~ print(Converter.mode)
		
	def updatetempcontrol(self):
		if Converter.tempctrl:
				Converter.tempctrl=0
				self.tempb['text']='OFF'
		else:
			Converter.tempctrl=1
			self.tempb['text']='ON'
		
		
	def updatevref(self,event):
		Converter.Vref=float(self.ventry.get())
		#~ print Converter.Vref
	
	def createnewwindowdevice(self,event):
		self.devicewindows.append(self.Devicewindow(self,self.ownwall[self.devicebuttons.index(event.widget)]))
	
	class Devicewindow:
		
		def __init__(self,parent,ownwall):
			
			self.ownwall=ownwall
			self.parent=parent
			self.mpage=tk.Tk()
			self.devicewindows=[]
			self.mpage.title('OwnWall')
			self.mpage.resizable(width=False,height=False)
			
			x=self.mpage.winfo_screenwidth()
			y=self.mpage.winfo_screenheight()
			self.mpage.geometry('+'+str(x/2-x/10)+'+'+str(y/2-y/6))
			
			self.mpframe=tk.LabelFrame(self.mpage,text=ownwall.port,labelanchor='n',font='TimesNewRoman 18',fg='white',bg='black',bd=5,pady=10,padx=10,relief='raised')
			self.mpframe.pack(expand=True,fill='both')
			self.mpframe.grid_rowconfigure(0, weight=1)
			self.mpframe.grid_columnconfigure(0, weight=1)
			self.mframestart()
			print threading.activeCount()
			threading.Thread(target=self.updatemeasures,args=()).start()
			self.mpage.mainloop()
			
			
			
		
		def mframestart(self):
			self.framesetup=tk.LabelFrame(self.mpframe,text='Setup',labelanchor='nw',font='TimesNewRoman 18',fg='white',bg='black',bd=5,pady=1,padx=5,relief='raised')
			self.framemeasures=tk.LabelFrame(self.mpframe,text='Measures',labelanchor='nw',font='TimesNewRoman 18',fg='white',bg='black',bd=5,pady=1,padx=5,relief='raised')
			self.framesetup.pack(side='left',expand=True,anchor='n',padx=5)
			self.framemeasures.pack(expand=True,anchor='w',side='left',padx=5)
			
			
			self.bidlabel=tk.Label(self.framesetup,text='Board ID',bg='black',fg='white',font='TimesNewRoman 14',pady=3)
			self.dclabel=tk.Label(self.framesetup,text='Duty-Cycle',bg='black',fg='white',font='TimesNewRoman 14',pady=3)
			self.tclabel=tk.Label(self.framesetup,text='Tracking Channel',bg='black',fg='white',font='TimesNewRoman 14',pady=3)
			self.tvlabel=tk.Label(self.framesetup,text='Setting',bg='black',fg='white',font='TimesNewRoman 14',pady=3)
			self.tlabel=tk.Label(self.framesetup,text='Tracking',bg='black',fg='white',font='TimesNewRoman 14',pady=3)
			self.pwmlabel=tk.Label(self.framesetup,text='PWM',bg='black',fg='white',font='TimesNewRoman 14',pady=3)
			self.vclabel=tk.Label(self.framesetup,text='Voltage Control',bg='black',fg='white',font='TimesNewRoman 14',pady=3)
			self.bidlabel.grid(row=0,column=0,sticky='e',padx=5)
			self.dclabel.grid(row=1,column=0,sticky='e',padx=5)
			self.tclabel.grid(row=2,column=0,sticky='e',padx=5)
			self.tvlabel.grid(row=3,column=0,sticky='e',padx=5)
			self.tlabel.grid(row=4,column=0,sticky='e',padx=5)
			self.pwmlabel.grid(row=5,column=0,sticky='e',padx=5)
			self.vclabel.grid(row=6,column=0,sticky='e',padx=5)
			
			self.bidentry=tk.Entry(self.framesetup,justify='center',width=10,bg='black',fg='white',insertbackground='white',highlightbackground='black')
			self.bidentry.grid(row=0,column=1)
			self.bidentry.insert(0,self.ownwall.board_id)
			self.bidentry.bind('<Return>',lambda event,a='board_id': self.updatesetup(event,a))
			
			self.dcentry=tk.Entry(self.framesetup,justify='center',width=10,bg='black',fg='white',insertbackground='white',highlightbackground='black')
			self.dcentry.grid(row=1,column=1)
			self.dcentry.insert(0,self.ownwall.duty_cycle)
			self.dcentry.bind('<Return>',lambda event,a='duty_cycle': self.updatesetup(event,a))
			
			self.setentry=tk.Entry(self.framesetup,justify='center',width=10,bg='black',fg='white',insertbackground='white',highlightbackground='black')
			self.setentry.grid(row=3,column=1)
			self.setentry.insert(0,self.ownwall.setting)
			self.setentry.bind('<Return>',lambda event,a='setting': self.updatesetup(event,a))
			
			self.trackchvar = tk.StringVar(self.framesetup)
			if self.ownwall.measure_cha>6:
				self.trackchvar.set('error')
			else:
				self.trackchvar.set(Converter.cha_name[self.ownwall.measure_cha]) # default value
			self.trackchmenu = apply(tk.OptionMenu, (self.framesetup, self.trackchvar) + tuple(Converter.cha_name[:6]))
			self.trackchmenu.configure(bg='black',fg='white',activebackground='black',activeforeground='white',indicatoron=0,bd=0,pady=2,padx=0,width=10)
			self.trackchmenu['menu'].configure(bg='black',fg='white',borderwidth=6,relief='flat')
			self.trackchmenu.grid(row=2,column=1)
			self.trackchvar.trace('w',lambda *args: self.updatetrackch())
						
			self.trackb=tk.Button(self.framesetup,bd=0,bg='black',pady=0,fg='white',font='Arial 10',width=10,padx=5,command=self.updatetrack)
			if self.ownwall.tracking_on:
				self.trackb['text']='ON'
			else:
				self.trackb['text']='OFF'
			self.trackb.grid(row=4,column=1)
			
			self.pwmb=tk.Button(self.framesetup,bd=0,bg='black',pady=0,fg='white',font='Arial 10',width=10,padx=5,command=self.updatepwm)
			if self.ownwall.pwm_run:
				self.pwmb['text']='ON'
			else:
				self.pwmb['text']='OFF'
			self.pwmb.grid(row=5,column=1)
			
			self.vcb=tk.Button(self.framesetup,bd=0,bg='black',pady=0,fg='white',font='Arial 10',width=10,padx=5,command=self.updatevc)
			if self.ownwall.voltage_control:
				self.vcb['text']='ON'
			else:
				self.vcb['text']='OFF'
			self.vcb.grid(row=6,column=1)
			
			self.varlabel=tk.Label(self.framemeasures,text='VARIABLE',bg='black',fg='white',bd=2,relief='raise',font='TimesNewRoman 14',pady=3,padx=5)
			self.callabel=tk.Label(self.framemeasures,text='CALIBER',bg='black',fg='white',bd=2,relief='raise',font='TimesNewRoman 14',pady=3,padx=5)
			self.offlabel=tk.Label(self.framemeasures,text='OFFSET',bg='black',fg='white',bd=2,relief='raise',font='TimesNewRoman 14',pady=3,padx=5)
			self.valuelabel=tk.Label(self.framemeasures,text='VALUE',bg='black',fg='white',bd=2,relief='raise',font='TimesNewRoman 14',pady=3,padx=25)
			self.illabel=tk.Label(self.framemeasures,text='iL',bg='black',fg='white',font='TimesNewRoman 14',pady=3,width=11)
			self.ihlabel=tk.Label(self.framemeasures,text='iH',bg='black',fg='white',font='TimesNewRoman 14',pady=3,width=11)
			self.vllabel=tk.Label(self.framemeasures,text='VL',bg='black',fg='white',font='TimesNewRoman 14',pady=3,width=11)
			self.vhlabel=tk.Label(self.framemeasures,text='VH',bg='black',fg='white',font='TimesNewRoman 14',pady=3,width=11)
			self.tlabel=tk.Label(self.framemeasures,text='T',bg='black',fg='white',font='TimesNewRoman 14',pady=3,width=11)
			self.pgoodlabel=tk.Label(self.framemeasures,text='Pgood',bg='black',fg='white',font='TimesNewRoman 14',pady=3,width=11)
			self.PWMlabel=tk.Label(self.framemeasures,text='PWM',bg='black',fg='white',font='TimesNewRoman 14',pady=3,width=11)
			
			self.varlabel.grid(row=0,column=0,sticky='n',padx=1)
			self.callabel.grid(row=0,column=1,sticky='n',padx=1)
			self.offlabel.grid(row=0,column=2,sticky='n',padx=1)
			self.valuelabel.grid(row=0,column=3,sticky='n',padx=1)
			self.illabel.grid(row=1,column=0,sticky='w',padx=5,pady=1)
			self.ihlabel.grid(row=2,column=0,sticky='w',padx=5,pady=1)
			self.vllabel.grid(row=3,column=0,sticky='w',padx=5,pady=1)
			self.vhlabel.grid(row=4,column=0,sticky='w',padx=5,pady=1)
			self.tlabel.grid(row=5,column=0,sticky='w',padx=5,pady=1)
			self.pgoodlabel.grid(row=6,column=0,sticky='w',padx=5,pady=1)
			self.PWMlabel.grid(row=7,column=0,sticky='w',padx=5,pady=1)
			
			self.ilcalentry=tk.Entry(self.framemeasures,justify='center',width=9,bg='black',fg='white',insertbackground='white',highlightbackground='black',font='Arial 12')
			self.ilcalentry.grid(row=1,column=1)
			self.ilcalentry.insert(0,self.ownwall.vmil_cal)
			self.ilcalentry.bind('<Return>',lambda event,a='vmil_cal': self.updatesetup(event,a))
			
			self.ihcalentry=tk.Entry(self.framemeasures,justify='center',width=9,bg='black',fg='white',insertbackground='white',highlightbackground='black',font='Arial 12')
			self.ihcalentry.grid(row=2,column=1)
			self.ihcalentry.insert(0,self.ownwall.vmih_cal)
			self.ihcalentry.bind('<Return>',lambda event,a='vmih_cal': self.updatesetup(event,a))
			
			self.vlcalentry=tk.Entry(self.framemeasures,justify='center',width=9,bg='black',fg='white',insertbackground='white',highlightbackground='black',font='Arial 12')
			self.vlcalentry.grid(row=3,column=1)
			self.vlcalentry.insert(0,self.ownwall.vml_cal)
			self.vlcalentry.bind('<Return>',lambda event,a='vml_cal': self.updatesetup(event,a))
			
			self.vhcalentry=tk.Entry(self.framemeasures,justify='center',width=9,bg='black',fg='white',insertbackground='white',highlightbackground='black',font='Arial 12')
			self.vhcalentry.grid(row=4,column=1)
			self.vhcalentry.insert(0,self.ownwall.vmh_cal)
			self.vhcalentry.bind('<Return>',lambda event,a='vmh_cal': self.updatesetup(event,a))
			
			self.tcalentry=tk.Entry(self.framemeasures,justify='center',width=9,bg='black',fg='white',insertbackground='white',highlightbackground='black',font='Arial 12')
			self.tcalentry.grid(row=5,column=1)
			self.tcalentry.insert(0,self.ownwall.vmt1_cal)
			self.tcalentry.bind('<Return>',lambda event,a='vmt1_cal': self.updatesetup(event,a))
			
			self.iloffentry=tk.Entry(self.framemeasures,justify='center',width=9,bg='black',fg='white',insertbackground='white',highlightbackground='black',font='Arial 12')
			self.iloffentry.grid(row=1,column=2)
			self.iloffentry.insert(0,self.ownwall.vmil_offset)
			self.iloffentry.bind('<Return>',lambda event,a='vmil_offset': self.updatesetup(event,a))
			
			self.ihoffentry=tk.Entry(self.framemeasures,justify='center',width=9,bg='black',fg='white',insertbackground='white',highlightbackground='black',font='Arial 12')
			self.ihoffentry.grid(row=2,column=2)
			self.ihoffentry.insert(0,self.ownwall.vmih_offset)
			self.ihoffentry.bind('<Return>',lambda event,a='vmih_offset': self.updatesetup(event,a))
			
			self.ilvlabel=tk.Label(self.framemeasures,text='0 '+Converter.cha_unit[Converter.cha_name.index('VmiL')],bg='black',fg='white',font='TimesNewRoman 14',pady=3,width=9)
			self.ihvlabel=tk.Label(self.framemeasures,text='0 '+Converter.cha_unit[Converter.cha_name.index('VmiH')],bg='black',fg='white',font='TimesNewRoman 14',pady=3,width=9)
			self.vlvlabel=tk.Label(self.framemeasures,text='0 '+Converter.cha_unit[Converter.cha_name.index('VmL')],bg='black',fg='white',font='TimesNewRoman 14',pady=3,width=9)
			self.vhvlabel=tk.Label(self.framemeasures,text='0 '+Converter.cha_unit[Converter.cha_name.index('VmH')],bg='black',fg='white',font='TimesNewRoman 14',pady=3,width=9)
			self.tvlabel=tk.Label(self.framemeasures,text='0 '+Converter.cha_unit[Converter.cha_name.index('VmT1')],bg='black',fg='white',font='TimesNewRoman 14',pady=3,width=9)
			self.pgoodvlabel=tk.Label(self.framemeasures,text='0 '+Converter.cha_unit[Converter.cha_name.index('Pgood')],bg='black',fg='white',font='TimesNewRoman 14',pady=3,width=9)
			self.PWMvlabel=tk.Label(self.framemeasures,text='0 '+Converter.cha_unit[Converter.cha_name.index('PWM')],bg='black',fg='white',font='TimesNewRoman 14',pady=3,width=9)
			
			self.ilvlabel.grid(row=1,column=3,sticky='n',padx=5,pady=1)
			self.ihvlabel.grid(row=2,column=3,sticky='n',padx=5,pady=1)
			self.vlvlabel.grid(row=3,column=3,sticky='n',padx=5,pady=1)
			self.vhvlabel.grid(row=4,column=3,sticky='n',padx=5,pady=1)
			self.tvlabel.grid(row=5,column=3,sticky='n',padx=5,pady=1)
			self.pgoodvlabel.grid(row=6,column=3,sticky='n',padx=5,pady=1)
			self.PWMvlabel.grid(row=7,column=3,sticky='n',padx=5,pady=1)
			
			#~ self.tcvclabel=tk.Label(self.framesetup,text='0',bg='black',fg='white',font='TimesNewRoman 14',pady=3)
			#~ self.tcvclabel.grid(row=2,column=1,sticky='n',padx=5)
			#~ self.setvclabel=tk.Label(self.framesetup,text='0',bg='black',fg='white',font='TimesNewRoman 14',pady=3)
			#~ self.setvclabel.grid(row=3,column=1,sticky='n',padx=5)
			#~ self.trackvclabel=tk.Label(self.framesetup,text='0',bg='black',fg='white',font='TimesNewRoman 14',pady=3)
			#~ self.trackvclabel.grid(row=4,column=1,sticky='n',padx=5)
			#~ self.pwmvclabel=tk.Label(self.framesetup,text='0',bg='black',fg='white',font='TimesNewRoman 14',pady=3)
			#~ self.pwmvclabel.grid(row=5,column=1,sticky='n',padx=5)
			
			
		
	
		def updatesetup(self,event,x):
			setattr(self.ownwall,x,int(event.widget.get()))
			while self.ownwall.sendingcmd:
				time.sleep(0.00001)
				pass
			self.ownwall.sendcmd(x,getattr(self.ownwall,x))
		
		def updatetrackch(self):
			self.ownwall.measure_cha=Converter.cha_name.index(self.trackchvar.get())
			while self.ownwall.sendingcmd:
				time.sleep(0.00001)
				pass
			self.ownwall.sendcmd('measure_cha',self.ownwall.measure_cha)
		
		def updatetrack(self):
			if self.ownwall.tracking_on:
				self.ownwall.tracking_on=0
				self.trackb['text']='OFF'
			else:
				self.ownwall.tracking_on=1
				self.trackb['text']='ON'
			while self.ownwall.sendingcmd:
				time.sleep(0.00001)
				pass
			self.ownwall.sendcmd('tracking_on',self.ownwall.tracking_on)
		
		def updatepwm(self):
			if self.ownwall.pwm_run:
				self.ownwall.pwm_run=0
				self.pwmb['text']='OFF'
			else:
				self.ownwall.pwm_run=1
				self.pwmb['text']='ON'
			while self.ownwall.sendingcmd:
				time.sleep(0.00001)
				pass
			self.ownwall.sendcmd('pwm_run',self.ownwall.pwm_run)
		
		def updatevc(self):
						
			if self.ownwall.voltage_control:
				self.ownwall.voltage_control=0
				self.vcb['text']='OFF'
				
				self.trackb['text']='ON'
				self.trackb.grid(row=4,column=1)
				self.pwmb['text']='ON'
				self.pwmb.grid(row=5,column=1)
				self.trackchvar.set(Converter.cha_name[2])
				self.trackchmenu.grid(row=2,column=1)
				self.dcentry.grid(row=1,column=1)
				self.dcentry.delete(0,'end')
				self.dcentry.insert(0,self.ownwall.duty_cycle)
				self.setentry.grid(row=3,column=1)
				self.setentry.delete(0,'end')
				self.setentry.insert(0,self.ownwall.setting)
				
			else:
				self.vcb['text']='ON'
				self.trackb.grid_forget()
				self.pwmb.grid_forget()
				self.trackchmenu.grid_forget()
				self.dcentry.grid_forget()
				self.setentry.grid_forget()
				
				self.ownwall.measure_cha=2
				while self.ownwall.sendingcmd:
					time.sleep(0.00001)
					pass
				self.ownwall.sendcmd('measure_cha',2)
				
				self.ownwall.pwm_run=1
				while self.ownwall.sendingcmd:
					time.sleep(0.00001)
					pass
				self.ownwall.sendcmd('pwm_run',1)
				
				self.ownwall.tracking_on=1
				while self.ownwall.sendingcmd:
					time.sleep(0.00001)
					pass
				self.ownwall.sendcmd('tracking_on',1)
				while self.ownwall.sendingcmd:
					time.sleep(0.00001)
					pass
				self.ownwall.voltage_control=1
		
		def updatemeasures(self):
										
			self.oldmeasures=[]
			self.newmeasures=[]
			
			print threading.activeCount()
			while 1:
				
				time.sleep(0.02)				
				while not self.ownwall.newdisplaymeasureflag:
					time.sleep(0.02)
					pass
				self.ownwall.newdisplaymeasureflag=0
				self.newmeasures=self.ownwall.displaymeasures
														
				if self.newmeasures != self.oldmeasures:
					self.oldmeasures=copy.deepcopy(self.newmeasures)
					self.ilvlabel['text']=str(round(self.newmeasures[2],2))+' '+Converter.cha_unit[Converter.cha_name.index('VmiL')]
					self.ihvlabel['text']=str(round(self.newmeasures[4],2))+' '+Converter.cha_unit[Converter.cha_name.index('VmiH')]
					self.vlvlabel['text']=str(round(self.newmeasures[1],2))+' '+Converter.cha_unit[Converter.cha_name.index('VmL')]
					self.vhvlabel['text']=str(round(self.newmeasures[5],2))+' '+Converter.cha_unit[Converter.cha_name.index('VmH')]
					self.tvlabel['text']=str(round(self.newmeasures[3],2))+' '+Converter.cha_unit[Converter.cha_name.index('VmT1')]
					self.pgoodvlabel['text']=str(round(self.newmeasures[0],2))+' '+Converter.cha_unit[Converter.cha_name.index('Pgood')]
					self.PWMvlabel['text']=str(round(self.newmeasures[6],2))+' '+Converter.cha_unit[Converter.cha_name.index('PWM')]
		
