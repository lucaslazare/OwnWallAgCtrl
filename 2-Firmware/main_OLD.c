/*
 * OwnWall main.c 
 * 
 *  Created on: 12 feb. 2018
 * 
 * Copyright 2018 hugues <hugues.larrive@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

//---------------------------------------------------------------------------------------
// GLOBAL INCLUDES
//---------------------------------------------------------------------------------------

#include <stdlib.h>
#include "pwm.h"
#include "rs232.h" 
#include <avr/eeprom.h>
#include "daq.h"
#include "spi.h"

//---------------------------------------------------------------------------------------
// GLOBAL VARIABLE IMPORTS
//---------------------------------------------------------------------------------------

/** global vars for pwm interrupt */
extern volatile uint8_t pwm_int[8];
extern volatile uint8_t pwm_idx;            // index for which pwm value is being run
extern volatile uint8_t timer_10us;         // flag for the end of the 10us timer period
extern volatile uint8_t pwm_run;            // on/off flag for the pwm
extern volatile uint8_t pwm_real_dt;        // real values of duty cycle for OCRnB
extern volatile uint8_t pwm_real_dt_higher; // pwm_real_dt + 1
extern volatile uint8_t pwm_mod;            // modulo
	

/** global vars for rs232 interrupts */
extern volatile uint8_t rs232_rx_buffer[4];  //imports the RS232 read buffer
extern volatile uint8_t rs232_rx_idx;        //imports the RS232 read index
extern volatile uint8_t rs232_rx_flag;       //imports the RS232 read flag
extern volatile uint8_t rs232_tx_buffer[50]; //imports the RS232 write buffer
extern volatile uint8_t rs232_tx_idx;        //imports the RS232 write index
extern volatile uint8_t rs232_tx_flag;       //imports the RS232 write flag
extern volatile uint8_t rs232_tx_len;        //imports the RS232 write vector length

/** global var for spi interrupts */
extern volatile uint8_t spi_buffer[3];       //imports the SPI buffer


//---------------------------------------------------------------------------------------
// FUNCTION - main
// DESCRIPTION - This main can be described as divided in two blocks.
//      ASYNCHRONOUS BLOCK - 
//                           This block receives instructions from a higher layer
//                           The instruction is interpreted according to the communication protocol
//                                   Command      b        |d          |e        |f           |g        |h           |i       |j       |k        |m          |p       |s       |t
//                                   RX len       2        |3          |3        |3           |3        |3           |3       |3       |3        |2          |2       |3       |2
//                                   Sets         board_id |duty_cycle |vmil_cal |vmil_offset |vmih_cal |vmih_offset |vml_cal |vmh_cal |vmt1_cal |mesure_cha |pwm_run |setting |tracking_on
//                           The asynchronous block treats the instruction at the end of every 8 blocks if it can be treated synchronously. 
//                           If not, the BRyC converter keeps its current setting until the instruction is treated
//              
//      SYNCHRONOUS BLOCK - 
//                           This block implements a scheduler
//             SCHEDULER - 
//                         The scheduler is built on 8 repetitive blocks of 10us
//                         Each block runs a part of three routines: 
//                         PWM modulation, Reference tracking control and Data acquisition/transmission   
//                 PWM MODULATION -
//                                  This is the fastest part of the code and happens at every period
//                                  There are 80 possible values for the PWM (0 - 79), which is too imprecise (1,6%)
//                                  To raise precision, a virtual PWM modulation was implemented
//                                  By using the 8 periods of 10us, the possible PWM values are multiplied by 8 (toatl of 640)
//                                  To reach one of these intermediary values, the target value is divided by 80, giving a bottom value (e.g. 510/640 = 63)
//                                  The modulo of this division is then calculated (510%8 = 6)
//                                  Over the 8 block period, the value above of the division is used the number of modulo (e.g.  64 - 64 - 64 - 64 - 64 - 64 - 63 - 63 )
//                                  This values are updated (regadless of the previous value) every period for the sake of code repeteability  
//                 REF. TRACKING - 
//                                  The reference tracking is a fast PID-like control used to track a certain variable being read by the BRyC converter
//                                  To track a reference, the code compares the measurement value with the reference
//                                  If the measurement is lower than the reference, the duty cycle value is raised by a certain step
//                                  If the measurement is higher than the reference, the duty cycle value is lowered by a certain step
//                                  Convergence is very fast (response time 1 ms) 
//                 DATA ACQ/TRANS - 
//                                  By far the slowest routine of the code, this routine is spread over 3.5ms
//                                  Data acquisition is divided in two blocks: tracking acq. and transmission acq.
//                                  
//                                  TRACKING ACQUISITION - 
//                                                         Tracking acquisition uses half of the blocks (4), spanning over 40us due to the response time of the SPI
//                                                         Tracking acquisition is repetitive and fast with its purpose being providing fast information for the reference tracking routine.                                   
//                                  TRANSMISSION ACQUISITION -
//                                                         Transmission acquisition acquires information on all the variables being measured by the BRyC converter (Low-side Current & Voltage, High-side Current & Voltage, Temperature and Pgood)     
//                                                         Transmission acquisition has the objective of averaging data over time and send it to the higher layer
//                                                         Each of the 6 variables are acquired in-turn every 80us and their values are averaged over 8 acquisitions (i.e. 80us * 6 = 480us for all variables  420us*8 = 3.84ms)
//                                                         Each variable is then averaged and uploaded onto the RS232 buffer for transmission
// 
//---------------------------------------------------------------------------------------

int main(void)
{
//---------------------------------------------------------------------------------------
// Initializes all the variables
//---------------------------------------------------------------------------------------
	/** rs232 vars */
	uint8_t rs232_fwd_ct = 0;
	uint8_t rs232_tx_fill_idx = 0;
	uint8_t rs232_tx_trig = 0;
	
	/** daq vars */
	uint16_t daq_accu[6] = {0};
	uint8_t	daq_accu_idx = 0, daq_prev_idx = 0;
	uint8_t daq_avg_ct = 0, daq_avg_idx = 0;
	uint8_t daq_avg_hight = 0, daq_avg_low = 0;
	
	/** realtime loop var */
	uint8_t height_step = 0;
	
	/** voltage tracking vars*/
	uint16_t mesure = 0xfff;

	/** Setup vars */
	uint8_t		board_id;		// an identifier for the board
	uint16_t	duty_cycle;		// initial duty_cycle
	uint8_t		mesure_cha; 	// channel of ADC to track on (2=VmL)
	uint16_t	setting;		// value tracked for mesure_cha (12*53,57)
	uint8_t		tracking_on;	// toggle tracking on or off

//---------------------------------------------------------------------------------------
// Initializes all the routines
//---------------------------------------------------------------------------------------
	/** initialisation */
	rs232_init(B57600, D8, PN, STP1);   // communication routine with the higher layer
	pwm_init();                         // pwm generation and modulation routine
	spi_init();                         // spi communication for data acquisition
	sei();	

//---------------------------------------------------------------------------------------
// Initializes the internal setup
//---------------------------------------------------------------------------------------
	/** reading setup from eeprom */
	board_id = eeprom_read_byte((uint8_t*)0);
	duty_cycle = eeprom_read_byte((uint8_t*)1)*256 + eeprom_read_byte((uint8_t*)2);
	mesure_cha = eeprom_read_byte((uint8_t*)3);
	setting = eeprom_read_byte((uint8_t*)4)*256 + eeprom_read_byte((uint8_t*)5);
	tracking_on = eeprom_read_byte((uint8_t*)6);
	pwm_run = eeprom_read_byte((uint8_t*)7);
	
	/** sending setup to software */
	rs232_tx_buffer[0] = board_id;
	rs232_tx_buffer[1] = (uint8_t)(duty_cycle>>8);
	rs232_tx_buffer[2] = (uint8_t)duty_cycle;
	rs232_tx_buffer[3] = mesure_cha;
	rs232_tx_buffer[4] = (uint8_t)(setting>>8);
	rs232_tx_buffer[5] = (uint8_t)setting;
	rs232_tx_buffer[6] = tracking_on;
	rs232_tx_buffer[7] = pwm_run;

	// copy calibration values
	for(uint16_t i=8; i<22; i++)
		rs232_tx_buffer[i] = eeprom_read_byte((uint8_t*)i);
	rs232_tx_len = 22;
	RS232_TX; // launch transmission
	while(rs232_tx_flag); // wait for transmission end
	UCSR0B &= ~(1 << TXCIE0); // disable TX interrupt

//---------------------------------------------------------------------------------------
// Initializes the debugging port
//---------------------------------------------------------------------------------------
	DDRC = 0xff; // PORT 0 of the Arduino dedicated for debugging
		
//---------------------------------------------------------------------------------------
// Main program block
//---------------------------------------------------------------------------------------
	while(1)
	{
        //---------------------------------------------------------------------------------------
        // SYNCHRONOUS BLOCK
        //---------------------------------------------------------------------------------------
		/** Mesures */
		if(timer_10us) // every 10us (step time)
		{
			//PORTC |= 1;
			
			timer_10us = 0; // reset the flag
			
            //---------------------------------------------------------------------------------------
            // TREATS THE 8 POSSIBLE BLOCKS
            //---------------------------------------------------------------------------------------
			switch(height_step)
			{
            //---------------------------------------------------------------------------------------
            // CASE 0 - 
            //  ROUTINE     FEEDBACK    |  DAQ  |
            //   STEP #        5/8      |   1   |
            //---------------------------------------------------------------------------------------
			case 0:
				//PORTC |= 1;
				
                //--------------------------------
				/** Feedback step 5 */
				mesure = DAQ_CATCH;                    // Catch a mesure for tracking
				
                //* duty cycle correction step 1 */
				if(tracking_on
				&& (mesure < setting)
				&& (duty_cycle < 639)) duty_cycle++;
				/** End feedback step 5 (1,8µs) */
				//-----------------------------------

				//PORTC &= ~1;
				//PORTC |= 1;
				
				//-----------------------------------
				/** DAQ step 1 */
				DAQ_LAUNCH(daq_accu_idx); // Launch mesure for DAQ using the buffer index. This measurement is intended for data transmission. Runs SPI1			
				//* loop over the six channels every 480µs */
				daq_prev_idx = daq_accu_idx;             // gets the DAQ index
				daq_accu_idx++;                          // updates the DAQ index
				if(daq_accu_idx == 6) daq_accu_idx = 0;  // resets the DAQ index
				/** End DAQ step 1 (1,8µs to 2µs) */
				//-----------------------------------

				//PORTC &= ~1;
				//* 3,4µs to 3,6µs from case 0
								
				height_step = 1;    //increments the height step
				break;
				
            //---------------------------------------------------------------------------------------
            // CASE 1 - 
            //  ROUTINE     FEEDBACK    |  DAQ  |
            //   STEP #        6/8      |   2   |
            //---------------------------------------------------------------------------------------
			case 1:
				//PORTC |= 1;
				
				//-----------------------------------
				/** Feedback step 6 */
				//** duty cycle correction step 2 */
				if(tracking_on)
				{
					if(mesure > setting && duty_cycle > 0) duty_cycle--;   // runs a tracking routine
					if(mesure==0) duty_cycle = 0;                          // cuts the PWM if there is no measurement
					//last_mesure = mesure;
				}
				//** setting new pwm duty cycle step 1 */
				PWM_SET1;                                                  // starts the PWM SET macro (1/4)
				/** End feedback step 6 (2,5µs) */
				//-----------------------------------
				
				//PORTC &= ~1;
				//PORTC |= 1;
				
				//-----------------------------------
				/** DAQ step 2 */
				SPI_2; // writing the second SPI command byte (the first was run by DAQ_LAUNCH)
				/** End DAQ step 2 (0,3µs) */
				//-----------------------------------
				
				
				//PORTC &= ~1;
				//* 2,7µs from case 1
								
				height_step = 2;    //increments the height step
				break;
				
            //---------------------------------------------------------------------------------------
            // CASE 2 - 
            //  ROUTINE     FEEDBACK    |  DAQ  |
            //   STEP #        7/8      |   3   |
            //---------------------------------------------------------------------------------------
			case 2:
				//PORTC |= 1;
				
				//-----------------------------------
				/** Feedback step 7 */
				//** setting new pwm duty cycle step 2 */
				PWM_SET2;                             // runs the second part of the PWM SET macro (2/4)
				/** End feedback step 7 (2,6µs) */
				//-----------------------------------
				
				//PORTC &= ~1;
				//PORTC |= 1;
				
				//-----------------------------------
				/** DAQ step 3 */
				SPI_3;                                 //runs the third part of the SPI macro (3/4)
				/** End DAQ step 3 (0,3µs) */
				//-----------------------------------
				
				//PORTC &= ~1;
				//* 2,8µs from case 2
				
				height_step = 3;
				break;
				
            //---------------------------------------------------------------------------------------
            // CASE 3 - 
            //  ROUTINE     FEEDBACK    |  DAQ  |
            //   STEP #        8/9      |   4   |
            //--------------------------------------------------------------------------------------
			case 3:
				//PORTC |= 1;
				
				//-----------------------------------
				/** Feedback step 8 */
				PWM_SET3;                            // runs the third part of the PWM SET macro (3/4)
				/** End feedback step 8 (3,2µs) */
				//-----------------------------------
				
				//PORTC &= ~1;
				//PORTC |= 1;
				
				//-----------------------------------
				/** DAQ step 4 */
				SPI_END;                                 //runs the last part of the SPI macro (4/4)
				/** End DAQ step 4 (0,4µs) */
				//-----------------------------------
				
				//PORTC &= ~1;
				//* 3,5µs from case 3
				
				height_step = 4;
				break;
				
            //---------------------------------------------------------------------------------------
            // CASE 4 - 
            //  ROUTINE     FEEDBACK    |  DAQ  |
            //   STEP #        1/8      |   5   |
            //--------------------------------------------------------------------------------------
			case 4:
				//PORTC |= 1;
				
				//-----------------------------------
				/** DAQ step 5 */
				daq_accu[daq_prev_idx] += DAQ_CATCH;          // retrieves the DAQ data (ultra-fast) and stores it on the DAQ buffer
				/** End DAQ step 5 (1,7µs) */
				
				//PORTC &= ~1;
				//PORTC |= 1;
				
				    //-----------------------------------
				    /** Feedback step 1 */
				    DAQ_LAUNCH(mesure_cha);                        // launches the DAQ with the measurement channel. This measurement in intended for PWM correction.
				    /** End feedback step 1 (1,4µs) */
				    //-----------------------------------
				
				//PORTC &= ~1;
				//PORTC |= 1;
				
				/** DAQ step 5 bis (delayed for DAQ_LAUNCH regularity) */
				if(daq_prev_idx == 5) daq_avg_ct++;                // increments the daq average counter
				/** End DAQ step 5 bis (0,3µs to 0,6µs) */
				//-----------------------------------

				//PORTC &= ~1;
				//* 3,3µs to 3,5µs from case 4
				
				height_step = 5;
				break;
				
            //---------------------------------------------------------------------------------------
            // CASE 5 - 
            //  ROUTINE     FEEDBACK       |  DAQ  |
            //   STEP #        9/9   2/9   |   6   |
            //--------------------------------------------------------------------------------------
			case 5:
				//PORTC |= 1;
				
				//-----------------------------------
				/** Feedback step 9 */
				PWM_SET4;
				/** End feedback step 9 (0,8µs) */
				//-----------------------------------
				
				//PORTC &= ~1;
				//PORTC |= 1;
				
				//-----------------------------------
				/** Feedback step 2 */
				SPI_2;
				/** End feedback step 2 (0,3µs) */
				//-----------------------------------
				
				//PORTC &= ~1;
				//PORTC |= 1;
				
				//-----------------------------------
				/** DAQ step 6 (every 3840µs) */
				//* Averaging 8 mesures per channel
				if(daq_avg_ct == 8)
				{
					// # >> 3 to divide by 8, 8+3=11 for msb
					daq_avg_hight = (uint8_t)(daq_accu[daq_avg_idx] >> 11);  // calculates the average of the last 4 bits (on a 12 bit measurement)
					daq_avg_low = (uint8_t)(daq_accu[daq_avg_idx] >> 3);     // calculates the average of the first 8 bits (on a 12 bit measurement)
					daq_accu[daq_avg_idx] = 0;                               // resets accumulator
					
				}
				/** End DAQ step 6 (0,3µ or 2,3µs) */
				//-----------------------------------
				
				//PORTC &= ~1;
				//* 1,2µs or 3,2µs from case 5
				
				height_step = 6;
				break;
				
            //---------------------------------------------------------------------------------------
            // CASE 6 - 
            //  ROUTINE    FEEDBACK |  DAQ  |
            //   STEP #        3/9   |   7   |
            //--------------------------------------------------------------------------------------
			case 6:
				//PORTC |= 1;

				//-----------------------------------
				/** DAQ step 7 */
				//** transfert raw mesure data into TX buffer */
				if(daq_avg_ct == 8)
				{
					rs232_tx_buffer[rs232_tx_fill_idx++] = daq_avg_hight;      // uploads high side - ---- XXXX
					rs232_tx_buffer[rs232_tx_fill_idx++] = daq_avg_low;        // uploads low side  - XXXX XXXX
					
					if(daq_avg_idx == 5)                                       // IF - all mesurements where buffered
					{
						rs232_tx_fill_idx = 0;                                    // resets the TX index
						
						/* buffering current PWM value */
						rs232_tx_buffer[12] = duty_cycle>>8;                      // uploads the duty cyle on the high side - ---- XXXX
						rs232_tx_buffer[13] = (uint8_t)(duty_cycle&0x00ff);       // uploads the duty cyle on the low side  - XXXX XXXX
						rs232_tx_trig = 1;                                        // triggers the sending of the TX
						
						daq_avg_idx = 0;                                          // resets index
						daq_avg_ct = 0;                                           // resets avg counter
					}
					else daq_avg_idx++;                                        // ELSE - increment index
				}
				/** End DAQ step 7 (0,3µs / 2,2µs / 2,6µs) */
				//-----------------------------------
				
				//PORTC &= ~1;
				//PORTC |= 1;
				
				//-----------------------------------
				/** Feedback step 3 */
				SPI_3;                                        // lauches SPI Macro step 3/4
				/** End feedback step 3 (0,4µs) */
				//-----------------------------------
				
				//PORTC &= ~1;
				//* 0,6µs / 2,5µs / 2,8µs from case 6
				
				height_step = 7;	
				break;
				
            //---------------------------------------------------------------------------------------
            // CASE 7 - 
            //  ROUTINE     FEEDBACK |  DAQ    |
            //   STEP #        3/9   |   8/8   |
            //--------------------------------------------------------------------------------------
			case 7:
				//PORTC |= 1;
				
				//-----------------------------------
				/** DAQ step 8 */
				//** forward serial transfert **/
				if(rs232_tx_flag && ++rs232_fwd_ct==3)   // test and increments the fwd counter
				{
					rs232_fwd_ct=0;
					RS232_FWD;
				}
				//** or launch new serial transfert **/
				if(rs232_tx_trig)
				{
						rs232_tx_trig = 0;
						rs232_tx_len = 16;
						RS232_TX;
				}
				/** End DAQ step 8 (0,6µs / 0,9µs / 2,4µs / 2,8µs) */
				//-----------------------------------
				
				//PORTC &= ~1;
				//PORTC |= 1;
				
				//-----------------------------------
				/** Feedback step 4 */
				SPI_END;                               //runs the 4/4 step of the SPI MACRO
				/** End feedback step 4 (0,5µs) */
				//-----------------------------------
				
				//PORTC &= ~1;
				//* 1µs to 3,1µs from case 7
				
				height_step = 0;
				break;
			} // end of switch(height_step)
		} // end if(timer_10us)		
		
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------
        // ASYNCHRONOUS BLOCK
        // Command      b        |d          |e        |f           |g        |h           |i       |j       |k        |m          |p       |s       |t
        // RX len       2        |3          |3        |3           |3        |3           |3       |3       |3        |2          |2       |3       |2
        // Sets         board_id |duty_cycle |vmil_cal |vmil_offset |vmih_cal |vmih_offset |vml_cal |vmh_cal |vmt1_cal |mesure_cha |pwm_run |setting |tracking_on
        // EPROM        0        | 1-2       | 8-9     | 10-11      | 12-13   | 14-15      | 16-17  | 18-19  | 20-21   | 3         | 7      | 4-5    | 6
        //--------------------------------------------------------------------------------------------------------------------------------------------------------------    
        
		/** receive settings from rs232 */
		if(rs232_rx_flag)
		{
			rs232_rx_flag = 0;	// reset rx flag

			switch(rs232_rx_buffer[0])
			{	
                //---------------------------------------------------------------------------------------
                // CASE b - SET BOARD ID
                //---------------------------------------------------------------------------------------
				case 'b': // set board_id
				PORTC |= 1;    // sets the PORTC to 1
					if(rs232_rx_idx==2)
					{
						rs232_rx_idx = 0;
						eeprom_write_byte((uint8_t*)0,rs232_rx_buffer[1]);
					}
					break;
				PORTC &= ~1;    // sets the PORTC to 1
				
                //---------------------------------------------------------------------------------------
                // CASE d - SET DUTY CYCLE
                // buffer[1] - ---- XXXX
                // buffer[2] - XXXX XXXX
                // PWM value = ---- XXXX XXXX XXXX
                //---------------------------------------------------------------------------------------
				case 'd': // set duty_cycle
					if(rs232_rx_idx==3)
					{
						rs232_rx_idx = 0;
						eeprom_write_byte((uint8_t*)1,rs232_rx_buffer[1]);
						eeprom_write_byte((uint8_t*)2,rs232_rx_buffer[2]);
						duty_cycle = rs232_rx_buffer[1]*256+rs232_rx_buffer[2];
						PWM_SET1;
						PWM_SET2;
						PWM_SET3;
						PWM_SET4;
					}
					break;
				
                //---------------------------------------------------------------------------------------
                // CASE m - SET MEASUREMENT CHANNEL
                // buffer[1] - measurement channel number
                //---------------------------------------------------------------------------------------
				case 'm': // set mesure_cha
					if(rs232_rx_idx==2)
					{
						rs232_rx_idx = 0;
						mesure_cha = rs232_rx_buffer[1];
						eeprom_write_byte((uint8_t*)3, mesure_cha);
					}
					break;
				
                //---------------------------------------------------------------------------------------
                // CASE s - SET SETTING	
                // buffer[1] - ?
                // buffer[2] - ?
                //---------------------------------------------------------------------------------------
				case 's': // set setting
					if(rs232_rx_idx==3)
					{
						rs232_rx_idx = 0;
						setting = rs232_rx_buffer[1]*256 + rs232_rx_buffer[2];
						eeprom_write_byte((uint8_t*)4, rs232_rx_buffer[1]);
						eeprom_write_byte((uint8_t*)5, rs232_rx_buffer[2]);
					}
					break;
				
                //---------------------------------------------------------------------------------------
                // CASE t - SET TRACKING ON	
                // buffer[1] - sets the tracking on or toggles tracking
                //---------------------------------------------------------------------------------------
                
				case 't': // set tracking_on
					if(rs232_rx_idx==2)
					{
						rs232_rx_idx = 0;
						tracking_on = rs232_rx_buffer[1];
						eeprom_write_byte((uint8_t*)6, tracking_on);
						// reset duty_cycle
						if(!tracking_on)
							duty_cycle =
								eeprom_read_byte((uint8_t*)1)*256
								+ eeprom_read_byte((uint8_t*)2);
					}
					break;
				
                //---------------------------------------------------------------------------------------
                // CASE p - SET PWM	RUN
                // buffer[1] - sets the pwm on or toggles pwm
                // reads the duty cycle on the EPROM and calls the PWM MACRO to launch the PWM
                //---------------------------------------------------------------------------------------
				case 'p': // set pwm_run
					if(rs232_rx_idx==2)
					{
						rs232_rx_idx = 0;
						pwm_run = rs232_rx_buffer[1];
						eeprom_write_byte((uint8_t*)7, pwm_run);
						if(pwm_run)
						{
							duty_cycle =
								eeprom_read_byte((uint8_t*)1)*256
								+ eeprom_read_byte((uint8_t*)2);
							PWM_SET1;
							PWM_SET2;
							PWM_SET3;
							PWM_SET4;
						}
						else pwm_stop();
					}
					break;				

                //---------------------------------------------------------------------------------------
                // CASE e - SET THE CALIBER OF VmiL 
                // buffer[1] - XXXX XXXX
                // buffer[2] - XXXX XXXX
                //---------------------------------------------------------------------------------------
				case 'e': // set vmil_cal
					if(rs232_rx_idx==3)
					{
						rs232_rx_idx = 0;
						eeprom_write_byte((uint8_t*)8, rs232_rx_buffer[1]);
						eeprom_write_byte((uint8_t*)9, rs232_rx_buffer[2]);
					}
					break;
				
                //---------------------------------------------------------------------------------------
                // CASE f - SET THE OFFSET OF VmiL 
                // buffer[1] - XXXX XXXX
                // buffer[2] - XXXX XXXX
                //---------------------------------------------------------------------------------------
				case 'f': // set vmil_offset
					if(rs232_rx_idx==3)
					{
						rs232_rx_idx = 0;
						eeprom_write_byte((uint8_t*)10, rs232_rx_buffer[1]);
						eeprom_write_byte((uint8_t*)11, rs232_rx_buffer[2]);
					}
					break;
				
                //---------------------------------------------------------------------------------------
                // CASE g - SET THE CALIBER OF VmiH 
                // buffer[1] - XXXX XXXX
                // buffer[2] - XXXX XXXX
                //---------------------------------------------------------------------------------------
				case 'g': // set vmih_cal
					if(rs232_rx_idx==3)
					{
						rs232_rx_idx = 0;
						eeprom_write_byte((uint8_t*)12, rs232_rx_buffer[1]);
						eeprom_write_byte((uint8_t*)13, rs232_rx_buffer[2]);
					}
					break;
				
                //---------------------------------------------------------------------------------------
                // CASE h - SET THE OFFSET OF VmiH 
                // buffer[1] - XXXX XXXX
                // buffer[2] - XXXX XXXX
                //---------------------------------------------------------------------------------------
				case 'h': // set vmih_offset
					if(rs232_rx_idx==3)
					{
						rs232_rx_idx = 0;
						eeprom_write_byte((uint8_t*)14, rs232_rx_buffer[1]);
						eeprom_write_byte((uint8_t*)15, rs232_rx_buffer[2]);
					}
					break;
				
                //---------------------------------------------------------------------------------------
                // CASE i - SET THE CALIBER OF VmL 
                // buffer[1] - XXXX XXXX
                // buffer[2] - XXXX XXXX
                //---------------------------------------------------------------------------------------
				case 'i': // set vml_cal
					if(rs232_rx_idx==3)
					{
						rs232_rx_idx = 0;
						eeprom_write_byte((uint8_t*)16, rs232_rx_buffer[1]);
						eeprom_write_byte((uint8_t*)17, rs232_rx_buffer[2]);
					}
					break;
				
                //---------------------------------------------------------------------------------------
                // CASE j - SET THE CALIBER OF VmH 
                // buffer[1] - XXXX XXXX
                // buffer[2] - XXXX XXXX
                //---------------------------------------------------------------------------------------
				case 'j': // set vmh_cal
					if(rs232_rx_idx==3)
					{
						rs232_rx_idx = 0;
						eeprom_write_byte((uint8_t*)18, rs232_rx_buffer[1]);
						eeprom_write_byte((uint8_t*)19, rs232_rx_buffer[2]);
					}
					break;
				
                //---------------------------------------------------------------------------------------
                // CASE k - SET THE CALIBER OF VmT1 
                // buffer[1] - XXXX XXXX
                // buffer[2] - XXXX XXXX
                //---------------------------------------------------------------------------------------
				case 'k': // set vmt1_cal
					if(rs232_rx_idx==3)
					{
						rs232_rx_idx = 0;
						eeprom_write_byte((uint8_t*)20, rs232_rx_buffer[1]);
						eeprom_write_byte((uint8_t*)21, rs232_rx_buffer[2]);
					}
					break;
				
			}
			// end switch(rs232_rx_buffer[0])			
		} 
		// end if(rs232_rx_flag)
	}
	// end while(1)
}
// end main()
