/*
 * mppt.h
 *
 *  Created on: 07th May 2018
 * 
 * Copyright 2018 Luiz Villa <luiz.villa@laas.fr>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
 
#ifndef MPPT_H_
#define MPPT_H_

#include <avr/io.h>

// defines the MPPT modes
#define BUCK  0		
#define BOOST 1		

// defines the MPPT states
#define MPPT_ON  1		
#define MPPT_OFF 0		


volatile uint16_t	current_now;
volatile uint16_t	voltage_now;
volatile uint16_t	power_now;
volatile uint16_t	power_before;
volatile uint8_t	mppt_sign;
volatile int8_t	    mppt_step;
volatile uint8_t	mppt_flag;				// flags a new mppt
volatile uint16_t	power_avg;				// averages power
volatile uint16_t	new_setting;		    // calculates the new setting

volatile uint8_t	mppt_counter;				// counts the mppt cycles
volatile uint8_t	mppt_avg;				// averages the mppt measurements (power of 2)
volatile uint16_t	mppt_buffer;				// holds the power of the MPPT


volatile uint8_t	voltage_Hbyte;
volatile uint8_t	voltage_Lbyte;
volatile uint8_t	current_Hbyte;
volatile uint8_t	current_Lbyte;



//~ volatile uint8_t pwm_mod;              // example variable

void mppt_init(uint8_t mppt_type, uint8_t step_size, uint8_t mppt_on);                   //function that initializes the PWM

//------------------------------------------------------------------
// MPPT SET MACRO
// Breaks the MPPT into 4 blocks
// Block MPPT_DATA_CATCH - retrieves the average current and voltage values
// Block MPPT_POWER_CALC - calculates the current power
// Block MPPT_SIGN_TOGGLE - toggles the MPPT corrector sign if needed
// Block MPPT_REF_CALC - calculates the new reference
//------------------------------------------------------------------

#define MPPT_1_DATA_CATCH\
    current_now = ((0x0f & rs232_tx_buffer[current_Hbyte])<<8) + rs232_tx_buffer[current_Lbyte];  /* Ultra-fast measurement retrieve technique*/ \
    voltage_now = ((0x0f & rs232_tx_buffer[voltage_Hbyte])<<8) + rs232_tx_buffer[voltage_Lbyte];  /* Ultra-fast measurement retrieve technique*/ \

#define MPPT_2_POWER_CALC\
	/* real values boundary check step 2 */\
	MULTIPLY(power_now, current_now, voltage_now); 		/* calculates the new power value*/\
    mppt_buffer += power_now>>mppt_avg;\
    mppt_counter++;    


#define MPPT_3_SIGN_TOGGLE\
	/* MPPT sign estimation and toggle */\	
    if(mppt_counter > 1<<mppt_avg ){\
        if(power_before > ( mppt_buffer + 4 ))                         /* compare power values*/\
        {\
            power_before = power_now; 							/* saves the previous power value*/\
            mppt_sign ^= 1 ; /* toggles the mppt_sign*/\
        }\
        power_before = mppt_buffer;\
        mppt_counter = 0;\
        mppt_buffer = 0;\
    }
	
#define MPPT_4_REF_CALC(setting)\
	/* MPPT sign estimation and toggle */\
    /*setting = setting + ( ( (mppt_sign << 1) - 1 ) << mppt_step ); /* does a reference step based on the mppt step (multiples of 2)*/\
    if(mppt_sign == 0){\
        new_setting = setting++ ;\
    }else{\
        new_setting = setting-- ;\
    }\
    /*setting = setting + mppt_step; /* does a reference step based on the mppt step (multiples of 2)*/\
    if(new_setting > 4096){\
        new_setting = 4096;\
    }




//------------------------------------------------------------------
// MULTIPLICATION MACRO FOR SPEED
// intRes = intIn1 * intIn2 >> 16
// uses:
// r26 to store 0
// r27 to store the byte 1 of the 32bit result
//------------------------------------------------------------------
#define MULTIPLY(intRes, intIn1, intIn2) \
	asm volatile ( \
	"clr r26 \n\t" \
	"mul %A1, %A2 \n\t" \
	"mov r27, r1 \n\t" \
	"mul %B1, %B2 \n\t" \
	"movw %A0, r0 \n\t" \
	"mul %B2, %A1 \n\t" \
	"add r27, r0 \n\t" \
	"adc %A0, r1 \n\t" \
	"adc %B0, r26 \n\t" \
	"mul %B1, %A2 \n\t" \
	"add r27, r0 \n\t" \
	"adc %A0, r1 \n\t" \
	"adc %B0, r26 \n\t" \
	"clr r1 \n\t" \
	: \
	"=&r" (intRes) \
	: \
	"a" (intIn1), \
	"a" (intIn2) \
	: \
	"r26" , "r27" \
	) 

//------------------------------------------------------------------
// ROUND MULTIPLICATION MACRO FOR SPEED
// intRes = intIn1 * intIn2 >> 16 + round
// uses:
// r26 to store 0
// r27 to store the byte 1 of the 32bit result
// 21 cycles
//------------------------------------------------------------------
#define ROUND_MULTIPLY(intRes, intIn1, intIn2) \
asm volatile ( \
"clr r26 \n\t" \
"mul %A1, %A2 \n\t" \
"mov r27, r1 \n\t" \
"mul %B1, %B2 \n\t" \
"movw %A0, r0 \n\t" \
"mul %B2, %A1 \n\t" \
"add r27, r0 \n\t" \
"adc %A0, r1 \n\t" \
"adc %B0, r26 \n\t" \
"mul %B1, %A2 \n\t" \
"add r27, r0 \n\t" \
"adc %A0, r1 \n\t" \
"adc %B0, r26 \n\t" \
"lsl r27 \n\t" \
"adc %A0, r26 \n\t" \
"adc %B0, r26 \n\t" \
"clr r1 \n\t" \
: \
"=&r" (intRes) \
: \
"a" (intIn1), \
"a" (intIn2) \
: \
"r26" , "r27" \
) 


//------------------------------------------------------------------
// ROUND MULTIPLICATION MACRO FOR SPEED
// longRes = intIn1 * intIn2
//------------------------------------------------------------------
#define MULTIPLY32(longRes, intIn1, intIn2) \
asm volatile ( \
"clr r26 \n\t" \
"mul %A1, %A2 \n\t" \
"movw %A0, r0 \n\t" \
"mul %B1, %B2 \n\t" \
"movw %C0, r0 \n\t" \
"mul %B2, %A1 \n\t" \
"add %B0, r0 \n\t" \
"adc %C0, r1 \n\t" \
"adc %D0, r26 \n\t" \
"mul %B1, %A2 \n\t" \
"add %B0, r0 \n\t" \
"adc %C0, r1 \n\t" \
"adc %D0, r26 \n\t" \
"clr r1 \n\t" \
: \
"=&r" (longRes) \
: \
"a" (intIn1), \
"a" (intIn2) \
: \
"r26" \
) 
	

#endif /* MPPT_H_ */
