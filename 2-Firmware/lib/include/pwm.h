/*
 * pwm.h
 *
 *  Created on: 12 feb. 2018
 * 
 * Copyright 2018 hugues <hugues.larrive@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
#ifndef PWM_H_
#define PWM_H_

#include <avr/io.h>
#include <avr/interrupt.h>

#define PWM_CT_TOP 80	// Couter/timer top value 16MHz/100K/2 = 80
#define PWM_DEAD_TIME 4 // The two outputs are low during 4/80 periode
#define PWM_MIN 8		
#define PWM_MAX 72		

volatile uint8_t pwm_int[8];   //vector that holds all the 8 values of PWM for a full cycle (80us)
volatile uint8_t pwm_idx;      //index of the PWM vector according to the blocks being executed
volatile uint8_t timer_10us;   //flag of the 10us interruption
volatile uint8_t pwm_run;      //flag that activates the PWM
volatile uint8_t pwm_real_dt;          // real values of duty cycle for OCRnB
volatile uint8_t pwm_real_dt_higher;   // pwm_real_dt + 1
volatile uint8_t pwm_mod;              // modulo

void pwm_init(void);                   //function that initializes the PWM
void pwm_stop(void);

//------------------------------------------------------------------
// PWM SET MACRO
// Breaks the SPI exchange into 4 blocks
// Block 1 - calculates the base value, the base value + 1, the modulus and checks the lower PWM limit
// Block 2 - checks the higher PWM limit, starts populating the PWM array
// Block 3 - keep populating the PWM array
// Block 4 - finishes populating the PWM array
//------------------------------------------------------------------

#define PWM_SET1\
	/* real values computing */\
	pwm_real_dt = duty_cycle / 8;\
	pwm_real_dt_higher = pwm_real_dt + 1;\
	pwm_mod = duty_cycle % 8;\
	\
	/* real values boundary check step 1*/\
	if(pwm_real_dt < PWM_MIN)\
	{\
		pwm_real_dt = PWM_MIN;\
		pwm_mod = 0;\
	}

#define PWM_SET2\
	/* real values boundary check step 2 */\
	if(pwm_real_dt > PWM_MAX)\
	{\
		pwm_real_dt = PWM_MAX;\
		pwm_mod = 0;\
	}\
	\
	/* circular array populating step 1 */\
	pwm_int[0] = pwm_mod>0 ? pwm_real_dt_higher : pwm_real_dt; /* 0,4µs */\
	pwm_int[1] = pwm_mod>1 ? pwm_real_dt_higher : pwm_real_dt;\
	pwm_int[2] = pwm_mod>2 ? pwm_real_dt_higher : pwm_real_dt;\

#define PWM_SET3\
	/* circular array populating step 2 */\
	pwm_int[3] = pwm_mod>3 ? pwm_real_dt_higher : pwm_real_dt;\
	pwm_int[4] = pwm_mod>4 ? pwm_real_dt_higher : pwm_real_dt;\
	pwm_int[5] = pwm_mod>5 ? pwm_real_dt_higher : pwm_real_dt;\
	pwm_int[6] = pwm_mod>6 ? pwm_real_dt_higher : pwm_real_dt;\
	
#define PWM_SET4\
	/* circular array populating step 3 */\
	pwm_int[7] = pwm_mod>7 ? pwm_real_dt_higher : pwm_real_dt;\
	

#endif /* PWM_H_ */
