/*
 * daq.h
 *
 *  Created on: 12 feb. 2018
 * 
 * Copyright 2018 hugues <hugues.larrive@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
#ifndef DAQ_H_
#define DAQ_H_

#include "spi.h"

/* From MCP3208 datasheet :
 * Because communication with the MCP3204/3208 devices may not need
 * multiples of eight clocks, it will be necessary to provide more
 * clocks than arerequired. This is usually done by sending ‘leading 
 * zeros’ before the start bit. [...] the first byte transmitted to
 * the A/D converter contains five leading zeros before the start bit.
 * Arranging the leading zeros this way allows the output 12 bits to
 * fall in positions easily manipulated by the MCU.
 * 
 * Board ver.       |
 * 4.0	| 4.1 | 4.2 |Chan|    start    sigl	D2	D1	D0|  Byte1	|  Byte2  |Hex1|Hex2
 * VmiL | VmiL|Pgood| 0  |	1	1	0	0	0 |0000 0110    |0000 0000|0x06|0x00
 * VmiH | VmiH|	VmL | 1  |	1	1	0	0	1 |0000 0110    |0100 0000|0x06|0x40
 * VmL  | VmL |	VmiL| 2  |	1	1	0	1	0 |0000 0110    |1000 0000|0x06|0x80
 * VmH  | VmH |	VmT1| 3  |	1	1	0	1	1 |0000 0110    |1100 0000|0x06|0xc0
 * VmT1 | VmT1|	VmiH| 4  |	1	1	1	0	0 |0000 0111    |0000 0000|0x07|0x00
 * VmT2 |Pgood|	VmH | 5  |	1	1	1	0	1 |0000 0111    |0100 0000|0x07|0x40
 * Pgood| NC  |	NC  | 6  |	1	1	1	1	0 |0000 0111    |1000 0000|0x07|0x80
 * NC	| NC  |	NC  | 7  |	1	1	1	1	1 |0000 0111    |1100 0000|0x07|0xc0
 */

//------------------------------------------------------------------
// DAQ MACRO
// Launches an SPI communication
//------------------------------------------------------------------


#define DAQ_LAUNCH(channel)\
do\
{\
	spi_buffer[0] = channel; /*defines the channel to the read*/ \
	spi_buffer[1] = 0x06 | channel>>2; /*defines the first channel register to be read*/ \
	spi_buffer[2] = channel<<6; /*defines the second channel register to be read*/ \
	SPI_START;\
}\
while(0)

#define DAQ_CATCH ((0x0f & spi_buffer[1])<<8) + spi_buffer[2];  /* Ultra-fast measurement retrieve technique*/

#endif /* DAQ_H_ */
