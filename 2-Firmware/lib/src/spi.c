/*
 * spi.c
 *
 *  Created on: 12 feb. 2018
 * 
 * Copyright 2018 hugues <hugues.larrive@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#include "spi.h"

//-------------------------------------------------------------
// FUNCTION - spi_init 
// DESCRIPTION - initializes the SPI communication
//-------------------------------------------------------------

void spi_init(void)
{
	uint8_t i;    // SPI counter 
	
	/* SPI pinout : SS=>PB2 ; MOSI=>PB3 ; MISO<=PB4 ; SCK=>PB5 */
	DDRB |= (1<<2)|(1<<3)|(1<<5); // outputs
	DDRB &= ~(1<<4); // input
	
	/* Interrupt Enable, SPI Enable, Master, Mode 1, clock rate fck/4 */
	SPCR = /*(1<<SPIE)|*/(1<<SPE)|(1<<MSTR)|(1<<CPHA);
	SPSR |= (1<<SPI2X); // double speed : fck/2

	for(i=0; i<3; i++) spi_buffer[i] = 0; // clear spi_buffer array
	
	PORTB |= (1<<2); // !SS high
}
