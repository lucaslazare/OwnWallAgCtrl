/*
 * mppt.c
 *
 *  Created on: 07th May 2018
 * 
 * Copyright 2018 Luiz Villa <luiz.villa@laas.fr>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#include "mppt.h"

void mppt_init(uint8_t mppt_type, uint8_t step_size, uint8_t mppt_on)
{                   //function that initializes the PWM

	// Initializes the MPPT variables
	current_now  = 0;             //sets the initial recent current value 
	voltage_now  = 0;             //sets the initial recent voltage value
	power_now    = 0;               //sets the initial recent power value
	power_before = 0;            //sets the initial old power value
	mppt_sign    = 0;               //sets the initial mppt sign (0 or 1)
	mppt_counter = 0;               //sets the initial mppt sign (0 or 1)
	mppt_buffer  = 0;               //sets the initial mppt sign (0 or 1)
    mppt_avg     = 3;               //sets the mppt average (power of 2)
	mppt_step    = step_size;               //sets the initial mppt step (a << variable, so a power of 2)
	mppt_flag    = mppt_on;			   //sets the initial mppt flag
	
	if(mppt_type == BUCK){               // BUCK MPPT - Tracks the LOW side
		voltage_Hbyte = 2;           //imports the location of the high byte of VmL value in the rs232 transmission buffer
		voltage_Lbyte = 3;           //imports the location of the low byte of VmL value in the rs232 transmission buffer
		current_Hbyte = 4;           //imports the location of the high byte of VmiL value in the rs232 transmission buffer
		current_Lbyte = 5;           //imports the location of the low byte of VmiL in the rs232 transmission buffer
    } 
    else                                // BOOST MPPT - Tracks the HIGH side
    {
		voltage_Hbyte = 10;           //imports the location of the high byte of VmL value in the rs232 transmission buffer
		voltage_Lbyte = 11;           //imports the location of the low byte of VmL value in the rs232 transmission buffer
		current_Hbyte = 8;           //imports the location of the high byte of VmiL value in the rs232 transmission buffer
		current_Lbyte = 9;           //imports the location of the low byte of VmiL in the rs232 transmission buffer		
	}
}// end of MPPT_init



