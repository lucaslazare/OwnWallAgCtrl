/*
 * rs232.c
 * 
 * Modified OwnWall version
 *
 *  Created on: 10 oct. 2017
 * 
 * Copyright 2017-2018 hugues <hugues.larrive@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#include "rs232.h"

//-------------------------------------------------------------
// FUNCTION - rs232_init 
// DESCRIPTION - initializes the RS232 communication
//-------------------------------------------------------------

void rs232_init(enum BAUD baud, enum DATA data, enum PARITE parite, enum STOP stop)
{
	/* baud rate */
	UCSR0A &= ~(1 << U2X0);
	UBRR0H = baud >> 8;
	UBRR0L = baud;

	/* number of data bits */
	UCSR0B &= ~(1 << UCSZ02);	// Does not use 9-bit data
	if(data & 2)	UCSR0C |= (1 << UCSZ01);
	else			UCSR0C &= ~(1 << UCSZ01);
	if(data & 1)	UCSR0C |= (1 << UCSZ00);
	else			UCSR0C &= ~(1 << UCSZ00);

	/* Parity */
	if(parite & 2)	UCSR0C |= (1 << UPM01);
	else			UCSR0C &= ~(1 << UPM01);
	if(parite & 1)	UCSR0C |= (1 << UPM00);
	else			UCSR0C &= ~(1 << UPM00);

	/* Stop bits */
	if(stop)	UCSR0C |= (1 << USBS0);
	else		UCSR0C &= ~(1 << USBS0);

	UCSR0B |= (1 << TXEN0);	// Enable transmitter
	UCSR0B |= (1 << RXEN0);	// Enable receiver

	/* Interrupts */
	UCSR0B |= (1 << TXCIE0) | (1 << RXCIE0);
	// sei();			// set enable interrupt
	data = UDR0;	                // dummy read
	
	rs232_tx_flag = 0; // ready to transmit
	rs232_rx_flag = 0;
}


//-------------------------------------------------------------
// FUNCTION - USART READ INTERRUPTION 
// DESCRIPTION - sets the actions for reading from the USART 
//-------------------------------------------------------------
ISR(USART_RX_vect)
{
	rs232_rx_flag = 1;
	rs232_rx_idx &= 3;
	rs232_rx_buffer[rs232_rx_idx++] = UDR0;   
}

//-------------------------------------------------------------
// FUNCTION - USART TRANSMITION INTERRUPTION 
// DESCRIPTION - sets the actions for transmitting from the USART 
//-------------------------------------------------------------
ISR(USART_TX_vect)
{
	if(rs232_tx_idx < rs232_tx_len) UDR0 = rs232_tx_buffer[rs232_tx_idx++];   // adds stuff to the buffer if the transmission is not yet finished
	else
	{//resets the buffer
		rs232_tx_len = 0;
		rs232_tx_idx = 0;
		rs232_tx_flag = 0;
	}
}

