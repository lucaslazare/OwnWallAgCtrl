/*
 * OwnWall main.c 
 * 
 *  Created on: 12 feb. 2018
 * 
 * Copyright 2018 hugues <hugues.larrive@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

//---------------------------------------------------------------------------------------
// GLOBAL INCLUDES
//---------------------------------------------------------------------------------------

#include <stdlib.h>
#include <avr/eeprom.h>

#include "pwm.h"
#include "mppt.h"
#include "rs232.h" 
#include "daq.h"
#include "spi.h"

//---------------------------------------------------------------------------------------
// GLOBAL VARIABLE IMPORTS
//---------------------------------------------------------------------------------------

/** global vars for pwm interrupt */
extern volatile uint8_t pwm_int[8];
extern volatile uint8_t pwm_idx;            // index for which pwm value is being run
extern volatile uint8_t timer_10us;         // flag for the end of the 10us timer period
extern volatile uint8_t pwm_run;            // on/off flag for the pwm
extern volatile uint8_t pwm_real_dt;        // real values of duty cycle for OCRnB
extern volatile uint8_t pwm_real_dt_higher; // pwm_real_dt + 1
extern volatile uint8_t pwm_mod;            // modulo
	

/** global vars for rs232 interrupts */
extern volatile uint8_t rs232_rx_buffer[4];  //imports the RS232 read buffer
extern volatile uint8_t rs232_rx_idx;        //imports the RS232 read index
extern volatile uint8_t rs232_rx_flag;       //imports the RS232 read flag
extern volatile uint8_t rs232_tx_buffer[50]; //imports the RS232 write buffer
extern volatile uint8_t rs232_tx_idx;        //imports the RS232 write index
extern volatile uint8_t rs232_tx_flag;       //imports the RS232 write flag
extern volatile uint8_t rs232_tx_len;        //imports the RS232 write vector length

/** global var for spi interrupts */
extern volatile uint8_t spi_buffer[3];       //imports the SPI buffer

/** global var for MPPT */
volatile uint16_t	current_now;             //imports the variable that stores the recent current value 
volatile uint16_t	voltage_now;             //imports the variable that stores the recent voltage value
volatile uint16_t	power_now;               //imports the variable that stores the recent power value
volatile uint16_t	power_before;            //imports the variable that stores the old power value
volatile uint8_t	mppt_sign;               //imports the variable that stores the mppt sign (0 or 1)
volatile int8_t	    mppt_step;               //imports the variable that stores the mppt step (a << variable, so a power of 2)
volatile uint8_t	mppt_flag;				 //imports the variable that stores the mppt flag
volatile uint16_t	new_setting;			 //imports the variable that stores the new mppt setting

volatile uint8_t	mppt_counter;				// counts the mppt cycles
volatile uint8_t	mppt_avg;				// averages the mppt measurements (power of 2)
volatile uint16_t	mppt_buffer;				// holds the power of the MPPT

volatile uint8_t	voltage_Hbyte;           //imports the location of the high byte of the 16 bit voltage value in the rs232 transmission buffer
volatile uint8_t	voltage_Lbyte;           //imports the location of the low byte of the 16 bit voltage value in the rs232 transmission buffer
volatile uint8_t	current_Hbyte;           //imports the location of the high byte of the 16 bit current value in the rs232 transmission buffer
volatile uint8_t	current_Lbyte;           //imports the location of the low byte of the 16 bit current value in the rs232 transmission buffer



//---------------------------------------------------------------------------------------
// FUNCTION - main
// DESCRIPTION - This main can be described as divided in two blocks.
//      ASYNCHRONOUS BLOCK - 
//                           This block receives instructions from a higher layer
//                           The instruction is interpreted according to the communication protocol
//                                   Command      b        |d          |e        |f           |g        |h           |i       |j       |k        |m          |p       |s       |t
//                                   RX len       2        |3          |3        |3           |3        |3           |3       |3       |3        |2          |2       |3       |2
//                                   Sets         board_id |duty_cycle |vmil_cal |vmil_offset |vmih_cal |vmih_offset |vml_cal |vmh_cal |vmt1_cal |measure_cha |pwm_run |setting |tracking_on
//                           The asynchronous block treats the instruction at the end of every 8 blocks if it can be treated synchronously. 
//                           If not, the BRyC converter keeps its current setting until the instruction is treated
//              
//      SYNCHRONOUS BLOCK - 
//                           This block implements a scheduler
//             SCHEDULER - 
//                         The scheduler is built on 8 repetitive blocks of 10us
//                         Each block runs a part of three routines: 
//                         PWM modulation, Reference tracking control and Data acquisition/transmission   
//                 PWM MODULATION -
//                                  This is the fastest part of the code and happens at every period
//                                  There are 80 possible values for the PWM (0 - 79), which is too imprecise (1,6%)
//                                  To raise precision, a virtual PWM modulation was implemented
//                                  By using the 8 periods of 10us, the possible PWM values are multiplied by 8 (toatl of 640)
//                                  To reach one of these intermediary values, the target value is divided by 80, giving a bottom value (e.g. 510/640 = 63)
//                                  The modulo of this division is then calculated (510%8 = 6)
//                                  Over the 8 block period, the value above of the division is used the number of modulo (e.g.  64 - 64 - 64 - 64 - 64 - 64 - 63 - 63 )
//                                  This values are updated (regadless of the previous value) every period for the sake of code repeteability  
//                 REF. TRACKING - 
//                                  The reference tracking is a fast PID-like control used to track a certain variable being read by the BRyC converter
//                                  To track a reference, the code compares the measurement value with the reference
//                                  If the measurement is lower than the reference, the duty cycle value is raised by a certain step
//                                  If the measurement is higher than the reference, the duty cycle value is lowered by a certain step
//                                  Convergence is very fast (response time 1 ms) 
//                 DATA ACQ/TRANS - 
//                                  By far the slowest routine of the code, this routine is spread over 3.5ms
//                                  Data acquisition is divided in two blocks: tracking acq. and transmission acq.
//                                  
//                                  TRACKING ACQUISITION - 
//                                                         Tracking acquisition uses half of the blocks (4), spanning over 40us due to the response time of the SPI
//                                                         Tracking acquisition is repetitive and fast with its purpose being providing fast information for the reference tracking routine.                                   
//                                  TRANSMISSION ACQUISITION -
//                                                         Transmission acquisition acquires information on all the variables being measured by the BRyC converter (Low-side Current & Voltage, High-side Current & Voltage, Temperature and Pgood)     
//                                                         Transmission acquisition has the objective of averaging data over time and send it to the higher layer
//                                                         Each of the 6 variables are acquired in-turn every 80us and their values are averaged over 8 acquisitions (i.e. 80us * 6 = 480us for all variables  420us*8 = 3.84ms)
//                                                         Each variable is then averaged and uploaded onto the RS232 buffer for transmission
// 
//---------------------------------------------------------------------------------------

int main(void)
{
//---------------------------------------------------------------------------------------
// Initializes all the variables
//---------------------------------------------------------------------------------------
	/** rs232 vars */
	uint8_t rs232_fwd_ct = 0;
	uint8_t rs232_tx_fill_idx = 0;
	uint8_t rs232_tx_trig = 0;
	
	/** daq vars */
	uint16_t daq_accu[6] = {0};
	uint8_t	daq_accu_idx = 0, daq_prev_idx = 0;
	uint8_t daq_avg_ct = 0, daq_avg_idx = 0;
	uint8_t daq_avg_hight = 0, daq_avg_low = 0;
	
	/** realtime loop var */
	uint8_t height_step = 0;
	
	/** voltage tracking vars*/
	uint16_t measure = 0xfff;

	/** Setup vars */
	uint8_t		board_id;		// an identifier for the board
	uint16_t	duty_cycle;		// initial duty_cycle
	uint8_t		measure_cha; 	// channel of ADC to track on (2=VmL)
	uint16_t	setting;		// value tracked for measure_cha (12*53,57)
	uint8_t		tracking_on;	// toggle tracking on or off
	
	uint8_t		charge_control_on;	    // toggle charge control on or off
	
	uint8_t     lvoltage_cha = 1;       // defines the current and voltage channels - MUST BE CHANGED IF CHANNELS CHANGE!
	uint8_t     lcurrent_cha = 2;
	uint8_t     hvoltage_cha = 5;
	uint8_t     hcurrent_cha = 4;
	
	
    uint8_t     sin_counter = 0;



    uint16_t     sin_vector[250] = {3000,
 3000, 3000, 3000, 3000, 3127, 3127, 3127, 3127, 3127, 3253,
 3253, 3253, 3253, 3253, 3375, 3375, 3375, 3375, 3375, 3490,
 3490, 3490, 3490, 3490, 3598, 3598, 3598, 3598, 3598, 3695,
 3695, 3695, 3695, 3695, 3781, 3781, 3781, 3781, 3781, 3855,
 3855, 3855, 3855, 3855, 3914, 3914, 3914, 3914, 3914, 3958,
 3958, 3958, 3958, 3958, 3987, 3987, 3987, 3987, 3987, 3999,
 3999, 3999, 3999, 3999, 3995, 3995, 3995, 3995, 3995, 3974,
 3974, 3974, 3974, 3974, 3938, 3938, 3938, 3938, 3938, 3886,
 3886, 3886, 3886, 3886, 3820, 3820, 3820, 3820, 3820, 3740,
 3740, 3740, 3740, 3740, 3648, 3648, 3648, 3648, 3648, 3545,
 3545, 3545, 3545, 3545, 3433, 3433, 3433, 3433, 3433, 3315,
 3315, 3315, 3315, 3315, 3191, 3191, 3191, 3191, 3191, 3064,
 3064, 3064, 3064, 3064, 2935, 2935, 2935, 2935, 2935, 2808,
 2808, 2808, 2808, 2808, 2684, 2684, 2684, 2684, 2684, 2566,
 2566, 2566, 2566, 2566, 2454, 2454, 2454, 2454, 2454, 2351,
 2351, 2351, 2351, 2351, 2259, 2259, 2259, 2259, 2259, 2179,
 2179, 2179, 2179, 2179, 2113, 2113, 2113, 2113, 2113, 2061,
 2061, 2061, 2061, 2061, 2025, 2025, 2025, 2025, 2025, 2004,
 2004, 2004, 2004, 2004, 2000, 2000, 2000, 2000, 2000, 2012,
 2012, 2012, 2012, 2012, 2041, 2041, 2041, 2041, 2041, 2085,
 2085, 2085, 2085, 2085, 2144, 2144, 2144, 2144, 2144, 2218,
 2218, 2218, 2218, 2218, 2304, 2304, 2304, 2304, 2304, 2401,
 2401, 2401, 2401, 2401, 2509, 2509, 2509, 2509, 2509, 2624,
 2624, 2624, 2624, 2624, 2746, 2746, 2746, 2746, 2746, 2872,
 2872, 2872, 2872, 2872, 2999, 2999, 2999, 2999, 2999}; 
 
 
    //~ uint16_t     sin_vector[250] = {2450,
 //~ 2450, 2450, 2450, 2450, 2641, 2641, 2641, 2641, 2641, 2830,
 //~ 2830, 2830, 2830, 2830, 3012, 3012, 3012, 3012, 3012, 3186,
 //~ 3186, 3186, 3186, 3186, 3347, 3347, 3347, 3347, 3347, 3493,
 //~ 3493, 3493, 3493, 3493, 3622, 3622, 3622, 3622, 3622, 3732,
 //~ 3732, 3732, 3732, 3732, 3821, 3821, 3821, 3821, 3821, 3888,
 //~ 3888, 3888, 3888, 3888, 3930, 3930, 3930, 3930, 3930, 3949,
 //~ 3949, 3949, 3949, 3949, 3943, 3943, 3943, 3943, 3943, 3912,
 //~ 3912, 3912, 3912, 3912, 3857, 3857, 3857, 3857, 3857, 3779,
 //~ 3779, 3779, 3779, 3779, 3680, 3680, 3680, 3680, 3680, 3560,
 //~ 3560, 3560, 3560, 3560, 3422, 3422, 3422, 3422, 3422, 3268,
 //~ 3268, 3268, 3268, 3268, 3100, 3100, 3100, 3100, 3100, 2922,
 //~ 2922, 2922, 2922, 2922, 2736, 2736, 2736, 2736, 2736, 2546,
 //~ 2546, 2546, 2546, 2546, 2353, 2353, 2353, 2353, 2353, 2163,
 //~ 2163, 2163, 2163, 2163, 1977, 1977, 1977, 1977, 1977, 1799,
 //~ 1799, 1799, 1799, 1799, 1631, 1631, 1631, 1631, 1631, 1477,
 //~ 1477, 1477, 1477, 1477, 1339, 1339, 1339, 1339, 1339, 1219,
 //~ 1219, 1219, 1219, 1219, 1120, 1120, 1120, 1120, 1120, 1042,
 //~ 1042, 1042, 1042, 1042, 987, 987, 987, 987, 987, 956,
 //~ 956, 956, 956, 956, 950, 950, 950, 950, 950, 969,
 //~ 969, 969, 969, 969, 1011, 1011, 1011, 1011, 1011, 1078,
 //~ 1078, 1078, 1078, 1078, 1167, 1167, 1167, 1167, 1167, 1277,
 //~ 1277, 1277, 1277, 1277, 1406, 1406, 1406, 1406, 1406, 1552,
 //~ 1552, 1552, 1552, 1552, 1713, 1713, 1713, 1713, 1713, 1887,
 //~ 1887, 1887, 1887, 1887, 2069, 2069, 2069, 2069, 2069, 2258,
 //~ 2258, 2258, 2258, 2258, 2449, 2449, 2449, 2449, 2449}; 




    //~ uint16_t     sin_vector[250] = {1500,
 //~ 1500, 1500, 1500, 1500, 1551, 1551, 1551, 1551, 1551, 1601,
 //~ 1601, 1601, 1601, 1601, 1650, 1650, 1650, 1650, 1650, 1696,
 //~ 1696, 1696, 1696, 1696, 1739, 1739, 1739, 1739, 1739, 1778,
 //~ 1778, 1778, 1778, 1778, 1812, 1812, 1812, 1812, 1812, 1842,
 //~ 1842, 1842, 1842, 1842, 1865, 1865, 1865, 1865, 1865, 1883,
 //~ 1883, 1883, 1883, 1883, 1894, 1894, 1894, 1894, 1894, 1899,
 //~ 1899, 1899, 1899, 1899, 1898, 1898, 1898, 1898, 1898, 1889,
 //~ 1889, 1889, 1889, 1889, 1875, 1875, 1875, 1875, 1875, 1854,
 //~ 1854, 1854, 1854, 1854, 1828, 1828, 1828, 1828, 1828, 1796,
 //~ 1796, 1796, 1796, 1796, 1759, 1759, 1759, 1759, 1759, 1718,
 //~ 1718, 1718, 1718, 1718, 1673, 1673, 1673, 1673, 1673, 1626,
 //~ 1626, 1626, 1626, 1626, 1576, 1576, 1576, 1576, 1576, 1525,
 //~ 1525, 1525, 1525, 1525, 1474, 1474, 1474, 1474, 1474, 1423,
 //~ 1423, 1423, 1423, 1423, 1373, 1373, 1373, 1373, 1373, 1326,
 //~ 1326, 1326, 1326, 1326, 1281, 1281, 1281, 1281, 1281, 1240,
 //~ 1240, 1240, 1240, 1240, 1203, 1203, 1203, 1203, 1203, 1171,
 //~ 1171, 1171, 1171, 1171, 1145, 1145, 1145, 1145, 1145, 1124,
 //~ 1124, 1124, 1124, 1124, 1110, 1110, 1110, 1110, 1110, 1101,
 //~ 1101, 1101, 1101, 1101, 1100, 1100, 1100, 1100, 1100, 1105,
 //~ 1105, 1105, 1105, 1105, 1116, 1116, 1116, 1116, 1116, 1134,
 //~ 1134, 1134, 1134, 1134, 1157, 1157, 1157, 1157, 1157, 1187,
 //~ 1187, 1187, 1187, 1187, 1221, 1221, 1221, 1221, 1221, 1260,
 //~ 1260, 1260, 1260, 1260, 1303, 1303, 1303, 1303, 1303, 1349,
 //~ 1349, 1349, 1349, 1349, 1398, 1398, 1398, 1398, 1398, 1448,
 //~ 1448, 1448, 1448, 1448, 1500, 1500, 1500, 1500, 1500} ;  //vector for the second wave experiment
        
    //~ uint16_t     sin_vector[250] = {1100,
        //~ 1100, 1100, 1100, 1100, 1100, 1125, 1125, 1125, 1125, 1125, 1150,
 //~ 1150, 1150, 1150, 1150, 1175, 1175, 1175, 1175, 1175, 1198,
 //~ 1198, 1198, 1198, 1198, 1219, 1219, 1219, 1219, 1219, 1239,
 //~ 1239, 1239, 1239, 1239, 1256, 1256, 1256, 1256, 1256, 1271,
 //~ 1271, 1271, 1271, 1271, 1282, 1282, 1282, 1282, 1282, 1291,
 //~ 1291, 1291, 1291, 1291, 1297, 1297, 1297, 1297, 1297, 1299,
 //~ 1299, 1299, 1299, 1299, 1299, 1299, 1299, 1299, 1299, 1294,
 //~ 1294, 1294, 1294, 1294, 1287, 1287, 1287, 1287, 1287, 1277,
 //~ 1277, 1277, 1277, 1277, 1264, 1264, 1264, 1264, 1264, 1248,
 //~ 1248, 1248, 1248, 1248, 1229, 1229, 1229, 1229, 1229, 1209,
 //~ 1209, 1209, 1209, 1209, 1186, 1186, 1186, 1186, 1186, 1163,
 //~ 1163, 1163, 1163, 1163, 1138, 1138, 1138, 1138, 1138, 1112,
 //~ 1112, 1112, 1112, 1112, 1087, 1087, 1087, 1087, 1087, 1061,
 //~ 1061, 1061, 1061, 1061, 1036, 1036, 1036, 1036, 1036, 1013,
 //~ 1013, 1013, 1013, 1013, 990, 990, 990, 990, 990, 970,
 //~ 970, 970, 970, 970, 951, 951, 951, 951, 951, 935,
 //~ 935, 935, 935, 935, 922, 922, 922, 922, 922, 912,
 //~ 912, 912, 912, 912, 905, 905, 905, 905, 905, 900,
 //~ 900, 900, 900, 900, 900, 900, 900, 900, 900, 902,
 //~ 902, 902, 902, 902, 908, 908, 908, 908, 908, 917,
 //~ 917, 917, 917, 917, 928, 928, 928, 928, 928, 943,
 //~ 943, 943, 943, 943, 960, 960, 960, 960, 960, 980,
 //~ 980, 980, 980, 980, 1001, 1001, 1001, 1001, 1001, 1024,
 //~ 1024, 1024, 1024, 1024, 1049, 1049, 1049, 1049, 1049, 1074,
 //~ 1074, 1074, 1074, 1074, 1100, 1100, 1100, 1100, 1100}; 
        
                                     //~ 1024,
                                     //~ 1024, 1024, 1024, 1024, 1141, 1141, 1141, 1141, 1141, 1257,
                                     //~ 1257, 1257, 1257, 1257, 1369, 1369, 1369, 1369, 1369, 1476,
                                     //~ 1476, 1476, 1476, 1476, 1575, 1575, 1575, 1575, 1575, 1665,
                                     //~ 1665, 1665, 1665, 1665, 1744, 1744, 1744, 1744, 1744, 1812,
                                     //~ 1812, 1812, 1812, 1812, 1866, 1866, 1866, 1866, 1866, 1907,
                                     //~ 1907, 1907, 1907, 1907, 1933, 1933, 1933, 1933, 1933, 1945,
                                     //~ 1945, 1945, 1945, 1945, 1941, 1941, 1941, 1941, 1941, 1922,
                                     //~ 1922, 1922, 1922, 1922, 1888, 1888, 1888, 1888, 1888, 1841,
                                     //~ 1841, 1841, 1841, 1841, 1779, 1779, 1779, 1779, 1779, 1706,
                                     //~ 1706, 1706, 1706, 1706, 1621, 1621, 1621, 1621, 1621, 1526,
                                     //~ 1526, 1526, 1526, 1526, 1423, 1423, 1423, 1423, 1423, 1314,
                                     //~ 1314, 1314, 1314, 1314, 1200, 1200, 1200, 1200, 1200, 1083,
                                     //~ 1083, 1083, 1083, 1083, 964, 964, 964, 964, 964, 847,
                                     //~ 847, 847, 847, 847, 733, 733, 733, 733, 733, 624,
                                     //~ 624, 624, 624, 624, 521, 521, 521, 521, 521, 426,
                                     //~ 426, 426, 426, 426, 341, 341, 341, 341, 341, 268,
                                     //~ 268, 268, 268, 268, 206, 206, 206, 206, 206, 159,
                                     //~ 159, 159, 159, 159, 125, 125, 125, 125, 125, 106,
                                     //~ 106, 106, 106, 106, 102, 102, 102, 102, 102, 114,
                                     //~ 114, 114, 114, 114, 140, 140, 140, 140, 140, 181,
                                     //~ 181, 181, 181, 181, 235, 235, 235, 235, 235, 303,
                                     //~ 303, 303, 303, 303, 382, 382, 382, 382, 382, 472,
                                     //~ 472, 472, 472, 472, 571, 571, 571, 571, 571, 678,
                                     //~ 678, 678, 678, 678, 790, 790, 790, 790, 790, 906,
                                     //~ 906, 906, 906, 906, 1023, 1023, 1023, 1023, 1023 }; 



    //~ uint8_t     sin_vector[250] = { 320,
                                     //~ 327, 334, 341, 349, 356, 363, 370, 377, 384, 391, 398, 405, 412, 419, 426, 433, 439, 446, 452, 459, 465, 471, 477, 483, 489,
                                     //~ 495, 501, 506, 512, 517, 523, 528, 533, 537, 542, 547, 551, 555, 559, 563, 567, 571, 574, 577, 581, 584, 586, 589, 592, 594,
                                     //~ 596, 598, 600, 601, 603, 604, 605, 606, 607, 607, 607, 607, 607, 607, 607, 606, 605, 604, 603, 602, 601, 599, 597, 595, 593,
                                     //~ 590, 588, 585, 582, 579, 576, 572, 569, 565, 561, 557, 553, 549, 544, 540, 535, 530, 525, 520, 515, 509, 504, 498, 492, 486,
                                     //~ 480, 474, 468, 462, 456, 449, 443, 436, 429, 423, 416, 409, 402, 395, 388, 381, 374, 367, 359, 352, 345, 338, 330, 323, 316,
                                     //~ 309, 301, 294, 287, 280, 272, 265, 258, 251, 244, 237, 230, 223, 216, 210, 203, 196, 190, 183, 177, 171, 165, 159, 153, 147,
                                     //~ 141, 135, 130, 124, 119, 114, 109, 104, 99, 95, 90, 86, 82, 78, 74, 70, 67, 63, 60, 57, 54, 51, 49, 46, 44,
                                     //~ 42, 40, 38, 37, 36, 35, 34, 33, 32, 32, 32, 32, 32, 32, 32, 33, 34, 35, 36, 38, 39, 41, 43, 45, 47,
                                     //~ 50, 53, 55, 58, 62, 65, 68, 72, 76, 80, 84, 88, 92, 97, 102, 106, 111, 116, 122, 127, 133, 138, 144, 150, 156,
                                     //~ 162, 168, 174, 180, 187, 193, 200, 206, 213, 220, 227, 234, 241, 248, 255, 262, 269, 276, 283, 290, 298, 305, 312, 319}; 
            
	//~ uint8_t     charge_vector[] = { 64, 65, 66, 67, 69, 70, 71, 72, 74, 75, 76, 78, 79, 80, 81, 83, 84, 85, 87, 88, 89, 90, 92, 93, 94, 96, 97, 98, 99, 101, 
	                                //~ 102, 103, 105, 106, 107, 108, 110, 111, 112, 114, 115, 116, 117, 119, 120, 121, 123, 124, 125, 126, 128, 129, 130, 132, 
	                                //~ 133, 134, 135, 137, 138, 139, 140, 142, 143, 144, 146, 147, 148, 149, 151, 152, 153, 155, 156, 157, 158, 160, 161, 162, 
	                                //~ 164, 165, 166, 167, 169, 170, 171, 173, 174, 175, 176, 178, 179, 180, 182, 183, 184, 185, 187, 188, 189, 191, 192, 193, 
	                                //~ 194, 196, 197, 198, 200, 201, 202, 203, 205, 206, 207, 209, 210, 211, 212, 214, 215, 216, 217, 219, 220, 221, 223, 224, 
	                                //~ 225, 226, 228, 229, 230, 232, 233, 234, 235, 237, 238, 239, 241, 242, 243, 244, 246, 247, 248, 250, 251, 252, 253, 255, 
	                                //~ 256, 257, 259, 260, 261, 262, 264, 265, 266, 268, 269, 270, 271, 273, 274, 275, 277, 278, 279, 280, 282, 283, 284, 285, 
	                                //~ 287, 288, 289, 291, 292, 293, 294, 296, 297, 298, 300, 301, 302, 303, 305, 306, 307, 309, 310, 311, 312, 314, 315, 316, 
	                                //~ 318, 319, 320, 321, 323, 324, 325, 327, 328, 329, 330, 332, 333, 334, 336, 337, 338, 339, 341, 342, 343, 345, 346, 347, 
	                                //~ 348, 350, 351, 352, 354, 355, 356, 357, 359, 360, 361, 362, 364, 365, 366, 368, 369, 370, 371, 373, 374, 375, 377, 378, 
	                                //~ 379, 380, 382, 383, 384, 386, 387, 388, 389, 391, 392, 393, 395, 396, 397, 398, 400, 401, 402, 404, 405, 406, 407, 409, 
	                                //~ 410, 411, 413, 414, 415, 416, 418, 419, 420, 422, 423, 424, 425, 427, 428, 429, 430, 432, 433, 434, 436, 437, 438, 439, 
	                                //~ 441, 442, 443, 445, 446, 447, 448, 450, 451, 452, 454, 455, 456, 457, 459, 460, 461, 463, 464, 465, 466, 468, 469, 470, 
	                                //~ 472, 473, 474, 475, 477, 478, 479, 481, 482, 483, 484, 486, 487, 488, 490, 491, 492, 493, 495, 496, 497, 499, 500, 501, 
	                                //~ 502, 504, 505, 506, 507, 509, 510, 511, 513, 514, 515, 516, 518, 519, 520, 522, 523, 524, 525, 527, 528, 529, 531, 532,
	                                 //~ 533, 534, 536, 537, 538, 540, 541, 542, 543, 545, 546, 547, 549, 550, 551, 552, 554, 555, 556, 558, 559, 560, 561, 563, 
	                                 //~ 564, 565, 567, 568, 569, 570, 572, 573, 574, 576	};   // charge vector is used to implement a smarter charge control
	
	/** debug var*/
	
	uint8_t		rs485_tx_flag = 0;	        // rs485 flag
	uint8_t		rs485_rx_flag = 0;	        // rs485 flag
	uint16_t	rs485_period = 65000;	// rs485 period
	uint16_t	rs485_counter = 65000;	// rs485 counter

	
	

//---------------------------------------------------------------------------------------
// Initializes all the routines
//---------------------------------------------------------------------------------------
	/** initialisation */
	rs232_init(B57600, D8, PN, STP1);   // communication routine with the higher layer
	pwm_init();                         // pwm generation and modulation routine
	spi_init();                         // spi communication for data acquisition
    mppt_init(BUCK, 6, MPPT_OFF); 		// chooses buck mode, sets the MPPT step to 6 (64 -> 64/4096 = 1.5% | 64/2048 = 3% -> for the currents)
	sei();	

//---------------------------------------------------------------------------------------
// Initializes the internal setup
//---------------------------------------------------------------------------------------
	/** reading setup from eeprom */
	board_id    = eeprom_read_byte((uint8_t*)0);                                     // loads the board ID
	duty_cycle  = eeprom_read_byte((uint8_t*)1)*256 + eeprom_read_byte((uint8_t*)2); // loads the duty cycle
	measure_cha  = eeprom_read_byte((uint8_t*)3);                                     // loads the measurement channel
	setting     = eeprom_read_byte((uint8_t*)4)*256 + eeprom_read_byte((uint8_t*)5); // loads the reference
	tracking_on = eeprom_read_byte((uint8_t*)6);                                     // loads if tracking is ON
	pwm_run     = eeprom_read_byte((uint8_t*)7);                                     // loads if PWM is ON
	
//---------------------------------------------------------------------------------------
// Launches the first transmission
//---------------------------------------------------------------------------------------
	/** sending setup to backend */
	
		
	rs232_tx_buffer[0] = board_id;						// sends th board ID 
	rs232_tx_buffer[1] = (uint8_t)(duty_cycle>>8);      // sends the duty cycle high byte
	rs232_tx_buffer[2] = (uint8_t)duty_cycle;           // sends the duty cycle low byte
	rs232_tx_buffer[3] = measure_cha;                    // sends the measuremnt channel
	rs232_tx_buffer[4] = (uint8_t)(setting>>8);         // sends the reference high byte
	rs232_tx_buffer[5] = (uint8_t)setting;              // sends the reference low byte
	rs232_tx_buffer[6] = tracking_on;                   // sends the state of tracking (on or off)
	rs232_tx_buffer[7] = pwm_run;                       // sends the state of PWM (ON or OFF)

	// copy calibration values
	for(uint16_t i=8; i<22; i++) 
		rs232_tx_buffer[i] = eeprom_read_byte((uint8_t*)i); //buffer up calibration values
	rs232_tx_len = 22;                                      // defines the length of the transmission buffer
	RS232_TX;                                               // launch transmission
	while(rs232_tx_flag);  
	while(rs232_rx_flag==0);
	rs232_rx_flag=0;
	rs232_rx_idx=0;                         // wait for transmission end
	UCSR0B &= ~(1 << TXCIE0);                               // disable TX interrupt

//---------------------------------------------------------------------------------------
// Initializes the debugging port
//---------------------------------------------------------------------------------------
	DDRC = 0xff; // PORT 0 of the Arduino dedicated for debugging
	//~ DDRD = (1 << DDD2); // PORT D2 set as output
	//~ PORTD = (0 << PORTD2); // PORT D2 set to low (to receive through RS485) - 2018-05-02 - Luiz Villa

	PORTC |= 1;    // sets the PORTC to 1
		
//---------------------------------------------------------------------------------------
// Main program block
//---------------------------------------------------------------------------------------
	while(1)
	{
        //---------------------------------------------------------------------------------------
        // SYNCHRONOUS BLOCK
        //---------------------------------------------------------------------------------------
		/** measures */
		if(timer_10us) // every 10us (step time)
		{
			//PORTC |= 1;
			
			timer_10us = 0; // reset the flag
			
			//~ if(rs485_tx_flag)             //test if the rs485 is transmitting
			//~ {
				//~ rs485_counter--;          // counts down the trasnmission period
				//~ if(rs485_counter == 0)
				//~ {
					//~ rs485_tx_flag = 0;     // lower the flag
					//~ rs485_counter = rs485_period;  // resets the counter
					//~ PORTD = (0 << PORTD2); // PORT D2 set to low (to receive through RS485) - 2018-05-02 - Luiz Villa
				    //~ PORTC &= ~1;           // Sets PORT C back to 0
				//~ }				
			//~ }
			
			
            //---------------------------------------------------------------------------------------
            // TREATS THE 8 POSSIBLE BLOCKS
            //---------------------------------------------------------------------------------------
			switch(height_step)
			{
//---------------------------------------------------------------------------------------
// CASE 0 - 
//  ROUTINE     FEEDBACK    |  DAQ  |
//   STEP #        5/8      |   1   |
//---------------------------------------------------------------------------------------
			case 0:
				//~ PORTC |= 1;

                //--------------------------------
				//** MPPT step 2 */
				if(mppt_flag){
                    MPPT_2_POWER_CALC;
                    } 					// calculates the power for the MPPT					 
                //--------------------------------
				
                //--------------------------------
				/** Feedback step 5 */
				measure = DAQ_CATCH;                    // Catch a measure for tracking
				
                //* duty cycle correction step 1 */
				//~ if(tracking_on) setting = sin_vector[sin_counter++];   // changes the sin vector
                //~ if(sin_counter>249) sin_counter=0;
				if(tracking_on && (measure > setting) && (measure_cha==hvoltage_cha)) duty_cycle++;
				else if(tracking_on && (measure > setting)) duty_cycle--;   // setting for operating as a load (voltage control)
				//~ if(tracking_on && (measure < setting) && (duty_cycle < 639)) duty_cycle++;   // setting for operating as a source (voltage control)
				if(duty_cycle<100) duty_cycle = 100;
				if(duty_cycle>639) duty_cycle= 639;
				//~ if(tracking_on && (measure < setting) && (duty_cycle < 639) && (measure_cha == lcurrent_cha || measure_cha == hcurrent_cha) ) duty_cycle++;       // setting for operating as a load (charge controller) - current control
				//~ if(tracking_on && (measure > setting) && (duty_cycle < 639) && (measure_cha == lvoltage_cha || measure_cha == hvoltage_cha) ) duty_cycle++;       // setting for operating as a load (charge controller) - voltage control

				//~ if(tracking_on && (measure > setting) && (duty_cycle < 639) && (measure_cha == lcurrent_cha || measure_cha == hcurrent_cha) ) duty_cycle++;       // setting for operating as a source - current control
				//~ if(tracking_on && (measure < setting) && (duty_cycle < 639) && (measure_cha == lvoltage_cha || measure_cha == hvoltage_cha) ) duty_cycle++;       // setting for operating as a source - voltage control

				//~ if(tracking_on && (measure > setting) && (duty_cycle < 639)) charge_vector[measure - setting];       // setting for operating as a load (charge controller)
				//~ if(charge_control_on && (measure > setting) && (duty_cycle < 639)) duty_cycle++;  // when charge control is on, the measurement is above setting and the duty cycle is not saturated, the duty cycle rises
				/** End feedback step 5 (1,8µs) */
				//-----------------------------------

				//PORTC &= ~1;
				//PORTC |= 1;
				
				//-----------------------------------
				/** DAQ step 1 */
				DAQ_LAUNCH(daq_accu_idx); // Launch measure for DAQ using the buffer index. This measurement is intended for data transmission. Runs SPI1			
				//* loop over the six channels every 480µs */
				daq_prev_idx = daq_accu_idx;             // gets the DAQ index
				daq_accu_idx++;                          // updates the DAQ index
				if(daq_accu_idx == 6) daq_accu_idx = 0;  // resets the DAQ index
				/** End DAQ step 1 (1,8µs to 2µs) */
				//-----------------------------------

				//~ PORTC &= ~1;
				//* 3,4µs to 3,6µs from case 0
								
				height_step = 1;    //increments the height step
				break;
				
//---------------------------------------------------------------------------------------
// CASE 1 - 
//  ROUTINE     FEEDBACK    |  DAQ  |
//   STEP #        6/8      |   2   |
//---------------------------------------------------------------------------------------
			case 1:
				//PORTC |= 1;
				
				//-----------------------------------
				//** MPPT step 3 */
				if(mppt_flag) MPPT_3_SIGN_TOGGLE;
				//-----------------------------------
				
				
				//-----------------------------------
				/** Feedback step 6 */
				//** duty cycle correction step 2 */
				if(tracking_on)
				{
					//~ if(measure > setting && duty_cycle > 0 && (measure_cha == lvoltage_cha || measure_cha == hvoltage_cha) ) duty_cycle--;   // runs a tracking routine as a source - voltage control
					//~ if(measure < setting && duty_cycle > 0 && (measure_cha == lcurrent_cha || measure_cha == hcurrent_cha) ) duty_cycle--;   // runs a tracking routine as a source - current control

					//~ if(measure < setting && duty_cycle > 0 && (measure_cha == lvoltage_cha || measure_cha == hvoltage_cha) ) duty_cycle--;   // runs a tracking routine as a load - voltage control

					//~ if(measure > setting && duty_cycle > 0 ) duty_cycle--;   // runs a tracking routine as a load - voltage control
                    if(tracking_on && (measure < setting) && (measure_cha==hvoltage_cha)) duty_cycle--;
                    else if(measure < setting) duty_cycle++;   // runs a tracking routine as a load - voltage control
					if(duty_cycle>639) duty_cycle = 639;
					if(duty_cycle<100) duty_cycle= 100;
					//~ if(measure > setting && duty_cycle > 0 && (measure_cha == lcurrent_cha || measure_cha == hcurrent_cha) ) duty_cycle--;   // runs a tracking routine as a source - current control

					//~ if(measure==0) duty_cycle = 0;                          // cuts the PWM if there is no measurement
					//~ if(measure < setting) duty_cycle = 0;                          // cuts the PWM if there is on extra voltage
					//last_measure = measure;
				}
				//** setting new pwm duty cycle step 1 */
				PWM_SET1;                                                  // starts the PWM SET macro (1/4)
				/** End feedback step 6 (2,5µs) */
				//-----------------------------------
				
				//PORTC &= ~1;
				//PORTC |= 1;
				
				//-----------------------------------
				/** DAQ step 2 */
				SPI_2; // writing the second SPI command byte (the first was run by DAQ_LAUNCH)
				/** End DAQ step 2 (0,3µs) */
				//-----------------------------------
				
				
				//PORTC &= ~1;
				//* 2,7µs from case 1
								
				height_step = 2;    //increments the height step
				break;
				
//---------------------------------------------------------------------------------------
// CASE 2 - 
//  ROUTINE     FEEDBACK    |  DAQ  |
//   STEP #        7/8      |   3   |
//---------------------------------------------------------------------------------------
			case 2:
				PORTC |= 1;
				
				if(mppt_flag && (mppt_counter == 0)){ 
                    
                    if( mppt_sign ){
                        setting++;
                    }else{
                        setting--;
                    }
                    
                    if(setting >= 4096){
                        setting = 4096; 
                        mppt_sign ^= 1 ; /* toggles the mppt_sign*/\
                    }
                    //~ setting = new_setting;        // uploads the new setting
                }
                
                PORTC &= ~1;
				
				//-----------------------------------
				/** Feedback step 7 */
				//** setting new pwm duty cycle step 2 */
				PWM_SET2;                             // runs the second part of the PWM SET macro (2/4)
				/** End feedback step 7 (2,6µs) */
				//-----------------------------------
				
				//~ PORTC &= ~1;
				//PORTC |= 1;
				
				//-----------------------------------
				/** DAQ step 3 */
				SPI_3;                                 //runs the third part of the SPI macro (3/4)
				/** End DAQ step 3 (0,3µs) */
				//-----------------------------------
				
				//PORTC &= ~1;
				//* 2,8µs from case 2
				
				height_step = 3;
				break;
				
//---------------------------------------------------------------------------------------
// CASE 3 - 
//  ROUTINE     FEEDBACK    |  DAQ  |
//   STEP #        8/9      |   4   |
//--------------------------------------------------------------------------------------
			case 3:
				//PORTC |= 1;
				
				//-----------------------------------
				/** Feedback step 8 */
				PWM_SET3;                            // runs the third part of the PWM SET macro (3/4)
				/** End feedback step 8 (3,2µs) */
				//-----------------------------------
				
				//PORTC &= ~1;
				//PORTC |= 1;
				
				//-----------------------------------
				/** DAQ step 4 */
				SPI_END;                                 //runs the last part of the SPI macro (4/4)
				/** End DAQ step 4 (0,4µs) */
				//-----------------------------------
				
				//PORTC &= ~1;
				//* 3,5µs from case 3
				
				height_step = 4;
				break;
				
//---------------------------------------------------------------------------------------
// CASE 4 - 
//  ROUTINE     FEEDBACK    |  DAQ  |
//   STEP #        1/8      |   5   |
//--------------------------------------------------------------------------------------
			case 4:
				//PORTC |= 1;
				
				//-----------------------------------
				/** DAQ step 5 */
				daq_accu[daq_prev_idx] += DAQ_CATCH;          // retrieves the DAQ data (ultra-fast) and stores it on the DAQ buffer
				/** End DAQ step 5 (1,7µs) */
				
				//PORTC &= ~1;
				//PORTC |= 1;
				
				    //-----------------------------------
				    /** Feedback step 1 */
				    DAQ_LAUNCH(measure_cha);                        // launches the DAQ with the measurement channel. This measurement in intended for PWM correction.
				    /** End feedback step 1 (1,4µs) */
				    //-----------------------------------
				
				//PORTC &= ~1;
				//PORTC |= 1;
				
				/** DAQ step 5 bis (delayed for DAQ_LAUNCH regularity) */
				if(daq_prev_idx == 5) daq_avg_ct++;                // increments the daq average counter
				/** End DAQ step 5 bis (0,3µs to 0,6µs) */
				//-----------------------------------

				//PORTC &= ~1;
				//* 3,3µs to 3,5µs from case 4
				
				height_step = 5;
				break;
				
//---------------------------------------------------------------------------------------
// CASE 5 - 
//  ROUTINE     FEEDBACK       |  DAQ  |
//   STEP #        9/9   2/9   |   6   |
//--------------------------------------------------------------------------------------
			case 5:
				//PORTC |= 1;
				
				//-----------------------------------
				/** Feedback step 9 */
				PWM_SET4;
				/** End feedback step 9 (0,8µs) */
				//-----------------------------------
				
				//PORTC &= ~1;
				//PORTC |= 1;
				
				//-----------------------------------
				/** Feedback step 2 */
				SPI_2;
				/** End feedback step 2 (0,3µs) */
				//-----------------------------------
				
				//PORTC &= ~1;
				//PORTC |= 1;
				
				//-----------------------------------
				/** DAQ step 6 (every 3840µs) */
				//* Averaging 8 measures per channel
				if(daq_avg_ct == 8)
				{
					// # >> 3 to divide by 8, 8+3=11 for msb
					daq_avg_hight = (uint8_t)(daq_accu[daq_avg_idx] >> 11);  // calculates the average of the last 4 bits (on a 12 bit measurement)
					daq_avg_low = (uint8_t)(daq_accu[daq_avg_idx] >> 3);     // calculates the average of the first 8 bits (on a 12 bit measurement)
					daq_accu[daq_avg_idx] = 0;                               // resets accumulator
					
				}
				/** End DAQ step 6 (0,3µ or 2,3µs) */
				//-----------------------------------
				
				//PORTC &= ~1;
				//* 1,2µs or 3,2µs from case 5
				
				height_step = 6;
				break;
				
//---------------------------------------------------------------------------------------
// CASE 6 - 
//  ROUTINE    FEEDBACK |  DAQ  |
//   STEP #        3/9   |   7   |
//--------------------------------------------------------------------------------------
			case 6:
				//~ PORTC |= 1;

				//-----------------------------------
				/** DAQ step 7 */
				//** transfert raw measure data into TX buffer */
				if(daq_avg_ct == 8)
				{
					rs232_tx_buffer[rs232_tx_fill_idx++] = daq_avg_hight;      // uploads high side - ---- XXXX
					rs232_tx_buffer[rs232_tx_fill_idx++] = daq_avg_low;        // uploads low side  - XXXX XXXX
					
					if(daq_avg_idx == 5)                                       // IF - all measurements where buffered
					{
						rs232_tx_fill_idx = 0;                                    // resets the TX index
						
						/* buffering current PWM value */
						if(mppt_flag){ 
                            MPPT_1_DATA_CATCH; 
                            }                                         // gets the voltage and the current for the MPPT operations
                            
						//~ rs232_tx_buffer[8] = setting>>8;                      // uploads the duty cyle on the high side - ---- XXXX
						//~ rs232_tx_buffer[9] = (uint8_t)(setting&0xffff);       // uploads the duty cyle on the low side  - XXXX XXXX

						//~ rs232_tx_buffer[10] = mppt_sign>>8;                      // uploads the duty cyle on the high side - ---- XXXX
						//~ rs232_tx_buffer[11] = (uint8_t)(mppt_sign&0xffff);       // uploads the duty cyle on the low side  - XXXX XXXX

						rs232_tx_buffer[12] = duty_cycle>>8;                      // uploads the duty cyle on the high side - ---- XXXX
						rs232_tx_buffer[13] = (uint8_t)(duty_cycle&0xffff);       // uploads the duty cyle on the low side  - XXXX XXXX

						rs232_tx_buffer[00] = setting>>8;                      // Sends the current now from the MPPT for debugging purposes - Luiz Villa May 8th 2018
						rs232_tx_buffer[01] = (uint8_t)(setting&0x00ff);;       // Sends the current now from the MPPT for debugging purposes - Luiz Villa May 8th 2018
						rs232_tx_trig = 1;                                        // triggers the sending of the TX
						
						
						daq_avg_idx = 0;                                          // resets index
						daq_avg_ct = 0;                                           // resets avg counter
					}
					else daq_avg_idx++;                                        // ELSE - increment index
				}
				/** End DAQ step 7 (0,3µs / 2,2µs / 2,6µs) */
				//-----------------------------------
				
				//~ PORTC &= ~1;
				//PORTC |= 1;
				
				//-----------------------------------
				/** Feedback step 3 */
				SPI_3;                                        // lauches SPI Macro step 3/4
				/** End feedback step 3 (0,4µs) */
				//-----------------------------------
				
				//~ PORTC &= ~1;
				//* 0,6µs / 2,5µs / 2,8µs from case 6
				
				height_step = 7;	
				break;
				
//---------------------------------------------------------------------------------------
// CASE 7 - 
//  ROUTINE     FEEDBACK |  DAQ    |
//   STEP #        3/9   |   8/8   |
//--------------------------------------------------------------------------------------
			case 7:
				//PORTC |= 1;
				
				//-----------------------------------
				/** DAQ step 8 */
				//** forward serial transfert **/
				if(rs232_tx_flag && ++rs232_fwd_ct==3)   // test and increments the fwd counter
				{
					rs232_fwd_ct=0;
					RS232_FWD;
				}
				//** or launch new serial transfert **/
				if(rs232_tx_trig)
				{
					rs232_tx_trig = 0;
					rs232_tx_len = 16;
					RS232_TX;
				}
				/** End DAQ step 8 (0,6µs / 0,9µs / 2,4µs / 2,8µs) */
				//-----------------------------------
				
				//PORTC &= ~1;
				//PORTC |= 1;
				
				//-----------------------------------
				/** Feedback step 4 */
				SPI_END;                               //runs the 4/4 step of the SPI MACRO
				/** End feedback step 4 (0,5µs) */
				//-----------------------------------
				
				//PORTC &= ~1;
				//* 1µs to 3,1µs from case 7
				
				height_step = 0;
				break;
			} // end of switch(height_step)
		} // end if(timer_10us)		
		
//-------------------------------------------------------------------------------------------------------------------------------------------------------------
// ASYNCHRONOUS BLOCK
// Command      b        |d          |e        |f           |g        |h           |i       |j       |k        |m          |p       |s       |t
// RX len       2        |3          |3        |3           |3        |3           |3       |3       |3        |2          |2       |3       |2
// Sets         board_id |duty_cycle |vmil_cal |vmil_offset |vmih_cal |vmih_offset |vml_cal |vmh_cal |vmt1_cal |measure_cha |pwm_run |setting |tracking_on
// EPROM        0        | 1-2       | 8-9     | 10-11      | 12-13   | 14-15      | 16-17  | 18-19  | 20-21   | 3         | 7      | 4-5    | 6
//--------------------------------------------------------------------------------------------------------------------------------------------------------------    
        
		/** receive settings from rs232 */
		if(rs232_rx_flag)
		{
			rs232_rx_flag = 0;	// reset rx flag

			switch(rs232_rx_buffer[0])
			{	
                //---------------------------------------------------------------------------------------
                // CASE b - SET BOARD ID
                //---------------------------------------------------------------------------------------
				case 'b': // set board_id
					if(rs232_rx_idx==2)
					{
						rs232_rx_idx = 0;
						eeprom_write_byte((uint8_t*)0,rs232_rx_buffer[1]);
					}
					
					

					//~ PORTD = (1 << PORTD2); // PORT D2 set to high (to transmit through RS485) - 2018-05-02 - Luiz Villa
					//~ rs485_tx_flag = 1;   // set rs485 transmission flag to 1 

				    //~ PORTC &= ~1;    // sets the PORTC to 0
				    //~ PORTC |= 1;    // sets the PORTC to 1
					break;
				
                //---------------------------------------------------------------------------------------
                // CASE d - SET DUTY CYCLE
                // buffer[1] - ---- XXXX
                // buffer[2] - XXXX XXXX
                // PWM value = ---- XXXX XXXX XXXX
                //---------------------------------------------------------------------------------------
				case 'd': // set duty_cycle
					if(rs232_rx_idx==3)
					{
						rs232_rx_idx = 0;
						eeprom_write_byte((uint8_t*)1,rs232_rx_buffer[1]);
						eeprom_write_byte((uint8_t*)2,rs232_rx_buffer[2]);
						duty_cycle = rs232_rx_buffer[1]*256+rs232_rx_buffer[2];
						PWM_SET1;
						PWM_SET2;
						PWM_SET3;
						PWM_SET4;
					}
					
					PORTD = (0 << PORTD2); // PORT D2 set to low (to receive through RS485) - 2018-05-02 - Luiz Villa
      				PORTC |= 1;    // sets the PORTC to 1
					
					break;
				
                //---------------------------------------------------------------------------------------
                // CASE m - SET MEASUREMENT CHANNEL
                // buffer[1] - measurement channel number
                //---------------------------------------------------------------------------------------
				case 'm': // set measure_cha
					if(rs232_rx_idx==2)
					{
						rs232_rx_idx = 0;
						measure_cha = rs232_rx_buffer[1];
						eeprom_write_byte((uint8_t*)3, measure_cha);
					}
					break;
				
                //---------------------------------------------------------------------------------------
                // CASE s - SET SETTING	
                // buffer[1] - ?
                // buffer[2] - ?
                //---------------------------------------------------------------------------------------
				case 's': // set setting
					if(rs232_rx_idx==3)
					{
						rs232_rx_idx = 0;
						setting = rs232_rx_buffer[1]*256 + rs232_rx_buffer[2];
						eeprom_write_byte((uint8_t*)4, rs232_rx_buffer[1]);
						eeprom_write_byte((uint8_t*)5, rs232_rx_buffer[2]);
					}
					break;
				
                //---------------------------------------------------------------------------------------
                // CASE t - SET TRACKING ON	
                // buffer[1] - sets the tracking on or toggles tracking
                //---------------------------------------------------------------------------------------
                
				case 't': // set tracking_on
					if(rs232_rx_idx==2)
					{
						rs232_rx_idx = 0;
						tracking_on = rs232_rx_buffer[1];
						eeprom_write_byte((uint8_t*)6, tracking_on);
						// reset duty_cycle
						if(!tracking_on)
							duty_cycle =
								eeprom_read_byte((uint8_t*)1)*256
								+ eeprom_read_byte((uint8_t*)2);
					}
					break;
				
                //---------------------------------------------------------------------------------------
                // CASE p - SET PWM	RUN
                // buffer[1] - sets the pwm on or toggles pwm
                // reads the duty cycle on the EPROM and calls the PWM MACRO to launch the PWM
                //---------------------------------------------------------------------------------------
				case 'p': // set pwm_run
					if(rs232_rx_idx==2)
					{
						rs232_rx_idx = 0;
						pwm_run = rs232_rx_buffer[1];
						eeprom_write_byte((uint8_t*)7, pwm_run);
						if(pwm_run)
						{
							duty_cycle =
								eeprom_read_byte((uint8_t*)1)*256
								+ eeprom_read_byte((uint8_t*)2);
							PWM_SET1;
							PWM_SET2;
							PWM_SET3;
							PWM_SET4;
						}
						else pwm_stop();
					}
					break;				

                //---------------------------------------------------------------------------------------
                // CASE e - SET THE CALIBER OF VmiL 
                // buffer[1] - XXXX XXXX
                // buffer[2] - XXXX XXXX
                //---------------------------------------------------------------------------------------
				case 'e': // set vmil_cal
					if(rs232_rx_idx==3)
					{
						rs232_rx_idx = 0;
						eeprom_write_byte((uint8_t*)8, rs232_rx_buffer[1]);
						eeprom_write_byte((uint8_t*)9, rs232_rx_buffer[2]);
					}
					break;
				
                //---------------------------------------------------------------------------------------
                // CASE f - SET THE OFFSET OF VmiL 
                // buffer[1] - XXXX XXXX
                // buffer[2] - XXXX XXXX
                //---------------------------------------------------------------------------------------
				case 'f': // set vmil_offset
					if(rs232_rx_idx==3)
					{
						rs232_rx_idx = 0;
						eeprom_write_byte((uint8_t*)10, rs232_rx_buffer[1]);
						eeprom_write_byte((uint8_t*)11, rs232_rx_buffer[2]);
					}
					break;
				
                //---------------------------------------------------------------------------------------
                // CASE g - SET THE CALIBER OF VmiH 
                // buffer[1] - XXXX XXXX
                // buffer[2] - XXXX XXXX
                //---------------------------------------------------------------------------------------
				case 'g': // set vmih_cal
					if(rs232_rx_idx==3)
					{
						rs232_rx_idx = 0;
						eeprom_write_byte((uint8_t*)12, rs232_rx_buffer[1]);
						eeprom_write_byte((uint8_t*)13, rs232_rx_buffer[2]);
					}
					break;
				
                //---------------------------------------------------------------------------------------
                // CASE h - SET THE OFFSET OF VmiH 
                // buffer[1] - XXXX XXXX
                // buffer[2] - XXXX XXXX
                //---------------------------------------------------------------------------------------
				case 'h': // set vmih_offset
					if(rs232_rx_idx==3)
					{
						rs232_rx_idx = 0;
						eeprom_write_byte((uint8_t*)14, rs232_rx_buffer[1]);
						eeprom_write_byte((uint8_t*)15, rs232_rx_buffer[2]);
					}
					break;
				
                //---------------------------------------------------------------------------------------
                // CASE i - SET THE CALIBER OF VmL 
                // buffer[1] - XXXX XXXX
                // buffer[2] - XXXX XXXX
                //---------------------------------------------------------------------------------------
				case 'i': // set vml_cal
					if(rs232_rx_idx==3)
					{
						rs232_rx_idx = 0;
						eeprom_write_byte((uint8_t*)16, rs232_rx_buffer[1]);
						eeprom_write_byte((uint8_t*)17, rs232_rx_buffer[2]);
					}
					break;
				
                //---------------------------------------------------------------------------------------
                // CASE j - SET THE CALIBER OF VmH 
                // buffer[1] - XXXX XXXX
                // buffer[2] - XXXX XXXX
                //---------------------------------------------------------------------------------------
				case 'j': // set vmh_cal
					if(rs232_rx_idx==3)
					{
						rs232_rx_idx = 0;
						eeprom_write_byte((uint8_t*)18, rs232_rx_buffer[1]);
						eeprom_write_byte((uint8_t*)19, rs232_rx_buffer[2]);
					}
					break;
				
                //---------------------------------------------------------------------------------------
                // CASE k - SET THE CALIBER OF VmT1 
                // buffer[1] - XXXX XXXX
                // buffer[2] - XXXX XXXX
                //---------------------------------------------------------------------------------------
				case 'k': // set vmt1_cal
					if(rs232_rx_idx==3)
					{
						rs232_rx_idx = 0;
						eeprom_write_byte((uint8_t*)20, rs232_rx_buffer[1]);
						eeprom_write_byte((uint8_t*)21, rs232_rx_buffer[2]);
					}
					break;
				
			}
			// end switch(rs232_rx_buffer[0])			
		} 
		// end if(rs232_rx_flag)
	}
	// end while(1)
}
// end main()
