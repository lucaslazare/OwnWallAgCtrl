EESchema Schematic File Version 2
LIBS:Power_converter_V4-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Gajda_opto
LIBS:IRS_Driver
LIBS:sensors
LIBS:arduino_shieldsNCL
LIBS:Measurement_block-cache
LIBS:Measurement_block-rescue
LIBS:digital
LIBS:lm2672
LIBS:digital-cache
LIBS:feeder_block-cache
LIBS:module_ttl_rs485
LIBS:buffer_chain-cache
LIBS:driver_block-cache
LIBS:driver_block-rescue
LIBS:measurement_chain-cache
LIBS:measurement_chain_LV-cache
LIBS:microcontroller_block-cache
LIBS:non inverting buffer_chain-cache
LIBS:power_block-cache
LIBS:power_block-rescue
LIBS:quad_shottky_x2
LIBS:Power_converter_V4-cache
LIBS:Power_converter_V4.2-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 7
Title ""
Date "20 apr 2016"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 1000 1050 0    60   Input ~ 0
PWMH1
Text HLabel 1000 1300 0    60   Input ~ 0
PWML1
Text Notes 900  650  0    60   ~ 0
BLOCK INPUTS
Text Notes 900  800  0    60   ~ 0
----------------------------
Text Notes 8300 700  0    60   ~ 0
BLOCK OUTPUTS
Text Notes 8300 850  0    60   ~ 0
----------------------------
Text HLabel 9200 1000 2    60   Output ~ 0
VgH
$Comp
L IRS2186 DR1
U 1 1 5717CC5F
P 9075 4935
F 0 "DR1" H 9325 5635 60  0000 C CNN
F 1 "IRS2186" H 9075 4385 60  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 9075 4335 60  0001 C CNN
F 3 "https://www.infineon.com/dgdl/irs2186pbf.pdf?fileId=5546d462533600a40153567716c427ed" H 9075 4335 60  0001 C CNN
	1    9075 4935
	1    0    0    -1  
$EndComp
Text Label 2270 5185 0    60   ~ 0
PWMH
Text Label 2285 5485 0    60   ~ 0
PWML
NoConn ~ 8525 4835
NoConn ~ 9625 4985
NoConn ~ 9625 5135
NoConn ~ 9625 5285
NoConn ~ 9625 4385
Text Label 10425 4685 0    60   ~ 0
VgH
Text Label 10475 5785 0    60   ~ 0
VgL
Text HLabel 9200 1200 2    60   Output ~ 0
VsH
Text HLabel 9200 1400 2    60   Output ~ 0
VgL
Text HLabel 10540 1420 2    60   Output ~ 0
VsL
Text Label 10425 4835 0    60   ~ 0
VsH
Text Label 10475 5985 0    60   ~ 0
VsL
Text Label 1400 1050 0    60   ~ 0
PWMH
Text Label 1400 1300 0    60   ~ 0
PWML
Text HLabel 1000 1550 0    60   Input ~ 0
GNDPWM
Text Label 1400 1550 0    60   ~ 0
GNDPWM
Text Label 1600 5785 0    60   ~ 0
GNDPWM
Text Label 8400 1000 2    60   ~ 0
VgH
Text Label 8400 1400 2    60   ~ 0
VgL
Text Label 8400 1200 2    60   ~ 0
VsH
Text Label 9740 1420 2    60   ~ 0
VsL
Text Notes 1335 2075 0    60   ~ 0
DIGITAL ISOLATOR
Text Notes 785  2225 0    60   ~ 0
----------------------------
Text Notes 915  2965 0    60   ~ 0
The digital isolator chosen for this \nconverter is a Si862BB which\nhas a VCC of +5V and can easily go up \nto 100kHz for low power applications\n\nIt requires two bypass capacitors Cdi1 and Cd2.\nFor noisy settings, the datasheet recommends a \nseries resistance with inputs and outpus between 50 to 300
Text Notes 2960 6585 0    60   ~ 0
           <-\nLOW        \nVOLTAGE\nSIDE 
Text Notes 4060 6585 0    60   ~ 0
->           \n     HIGH\n     VOLTAGE\n     SIDE 
Text Notes 1695 6905 0    60   ~ 0
----------------------------
Text Notes 4005 6905 0    60   ~ 0
----------------------------
Text Notes 1655 3830 0    60   ~ 0
----------------------------
Text Notes 4055 3830 0    60   ~ 0
----------------------------
Text Notes 8575 2735 0    60   ~ 0
POWER DRIVER
Text Notes 8125 2785 0    60   ~ 0
----------------------------
Text Notes 8425 3585 0    60   ~ 0
The power driver chosen\nfor this converter is the \nIR2186 capable of driving\na transitor up to 4A at a time.\nThis solution comprises a \nbootstrap which allows driving \nboth the high and low side \ntransistors from the same IC.
Text Label 5930 4885 2    60   ~ 0
+5VHigh
Text Label 7625 5585 2    60   ~ 0
+15VHigh
Text HLabel 2350 1595 0    60   Input ~ 0
GNDPwR
Text Label 5240 5185 2    60   ~ 0
Hin
Text Label 5250 5485 2    60   ~ 0
Lin
Text HLabel 10545 980  2    60   Output ~ 0
Hin
Text HLabel 10545 1180 2    60   Output ~ 0
Lin
Text Label 9745 980  2    60   ~ 0
Hin
Text Label 9745 1180 2    60   ~ 0
Lin
Text Notes 3955 4680 1    60   ~ 0
WARNING: \nMASSES MUST BE SEPARATED!!!
Text HLabel 2350 1195 0    60   Input ~ 0
+5VHigh
Text HLabel 2350 1395 0    60   Input ~ 0
+15VHigh
Text Label 2750 1395 0    60   ~ 0
+15VHigh
Text Label 2750 1195 0    60   ~ 0
+5VHigh
Text Notes 3800 1300 0    60   ~ 0
This circuit connects the microcontroller \nto the power transistors. \nIt uses a digital isolator to separate the grounds from both sides.\nThe digital isolator has a non-inverter output.
Text Notes 3800 750  0    60   ~ 0
DETAILS OF THE DRIVER BLOCK
Text Notes 3800 850  0    60   ~ 0
-----------------------------------------------
Text Notes 3800 1550 0    60   ~ 0
Based on these components\nthe system operates at 100kHz
$Comp
L R Rdi1
U 1 1 59367A12
P 2790 5185
F 0 "Rdi1" V 2890 5185 50  0000 C CNN
F 1 "220" V 2790 5185 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 2720 5185 50  0001 C CNN
F 3 "" H 2790 5185 50  0000 C CNN
	1    2790 5185
	0    1    1    0   
$EndComp
$Comp
L R Rdi3
U 1 1 59368910
P 4865 5185
F 0 "Rdi3" V 4965 5185 50  0000 C CNN
F 1 "220" V 4865 5185 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 4795 5185 50  0001 C CNN
F 3 "" H 4865 5185 50  0000 C CNN
	1    4865 5185
	0    1    1    0   
$EndComp
$Comp
L R Rdi4
U 1 1 59368965
P 4870 5485
F 0 "Rdi4" V 4970 5485 50  0000 C CNN
F 1 "220" V 4870 5485 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 4800 5485 50  0001 C CNN
F 3 "" H 4870 5485 50  0000 C CNN
	1    4870 5485
	0    1    1    0   
$EndComp
$Comp
L C Cdr2
U 1 1 5936923B
P 8175 5135
F 0 "Cdr2" H 8200 5235 50  0000 L CNN
F 1 "100n" H 8200 5035 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 8213 4985 50  0001 C CNN
F 3 "" H 8175 5135 50  0000 C CNN
	1    8175 5135
	1    0    0    -1  
$EndComp
$Comp
L C Cdr1
U 1 1 5936952A
P 7525 4885
F 0 "Cdr1" H 7550 4985 50  0000 L CNN
F 1 "100n" H 7225 4835 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 7563 4735 50  0001 C CNN
F 3 "" H 7525 4885 50  0000 C CNN
	1    7525 4885
	1    0    0    -1  
$EndComp
$Comp
L C Cdr3
U 1 1 59369B13
P 10175 4335
F 0 "Cdr3" H 10200 4435 50  0000 L CNN
F 1 "500n" H 10200 4235 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 10213 4185 50  0001 C CNN
F 3 "" H 10175 4335 50  0000 C CNN
	1    10175 4335
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X01 TD1
U 1 1 599F39EC
P 6270 1975
F 0 "TD1" H 6270 2075 50  0000 C CNN
F 1 "CONN_01X01" V 6370 1975 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm" H 6270 1975 50  0001 C CNN
F 3 "" H 6270 1975 50  0000 C CNN
	1    6270 1975
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X01 TD2
U 1 1 599F3A3B
P 6270 2225
F 0 "TD2" H 6270 2325 50  0000 C CNN
F 1 "CONN_01X01" V 6370 2225 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm" H 6270 2225 50  0001 C CNN
F 3 "" H 6270 2225 50  0000 C CNN
	1    6270 2225
	-1   0    0    1   
$EndComp
Text Label 6770 1975 0    60   ~ 0
Hin
Text Label 6770 2225 0    60   ~ 0
Lin
$Comp
L R Rdi2
U 1 1 59367A7F
P 2790 5485
F 0 "Rdi2" V 2890 5485 50  0000 C CNN
F 1 "220" V 2790 5485 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 2720 5485 50  0001 C CNN
F 3 "" H 2790 5485 50  0000 C CNN
	1    2790 5485
	0    1    1    0   
$EndComp
Text Label 2550 1745 0    60   ~ 0
GNDPwR
Text Label 5945 5785 2    60   ~ 0
GNDPwR
Text Label 6925 4785 0    60   ~ 0
GNDPwR
Text Label 7025 6135 0    60   ~ 0
GNDPwR
$Comp
L Si862BB Diso2
U 1 1 5ABE0E85
P 3875 5275
F 0 "Diso2" H 4315 5815 60  0000 C CNN
F 1 "Si862BB" H 4265 4565 60  0000 C CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 3685 5105 60  0001 C CNN
F 3 "https://www.silabs.com/documents/public/data-sheets/si861x-2x-datasheet.pdf" H 3685 5105 60  0001 C CNN
	1    3875 5275
	1    0    0    -1  
$EndComp
$Comp
L C Cdi1
U 1 1 5ABE2366
P 2195 5355
F 0 "Cdi1" H 1910 5470 50  0000 L CNN
F 1 "100n" H 1895 5305 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 2233 5205 50  0001 C CNN
F 3 "" H 2195 5355 50  0000 C CNN
	1    2195 5355
	1    0    0    -1  
$EndComp
Text HLabel 2350 1010 0    60   Input ~ 0
+5VLow
Text Label 2750 1010 0    60   ~ 0
+5VLow
Text Label 1600 4885 0    60   ~ 0
+5VLow
$Comp
L C Cdi2
U 1 1 5ABE94CD
P 5495 5355
F 0 "Cdi2" H 5520 5455 50  0000 L CNN
F 1 "100n" H 5545 5255 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 5533 5205 50  0001 C CNN
F 3 "" H 5495 5355 50  0000 C CNN
	1    5495 5355
	1    0    0    -1  
$EndComp
Wire Notes Line
	700  1750 3550 1750
Wire Notes Line
	3550 1750 3550 650 
Wire Notes Line
	8105 1625 10955 1625
Wire Notes Line
	8100 1610 8100 510 
Wire Wire Line
	2270 5185 2640 5185
Wire Wire Line
	8085 4535 8525 4535
Wire Wire Line
	6925 4685 8525 4685
Wire Wire Line
	6925 4685 6925 4785
Wire Wire Line
	7025 4985 7025 6135
Wire Wire Line
	7025 4985 8525 4985
Wire Wire Line
	8525 5135 7175 5135
Wire Wire Line
	7175 5135 7175 5785
Wire Wire Line
	7175 5785 10475 5785
Wire Wire Line
	7525 5285 8525 5285
Wire Wire Line
	7725 3985 8875 3985
Wire Wire Line
	7725 3985 7725 5285
Connection ~ 7725 5285
Wire Wire Line
	9775 4535 9625 4535
Wire Wire Line
	9775 3985 9775 4535
Wire Wire Line
	9175 3985 9775 3985
Wire Wire Line
	10175 4035 10175 4185
Wire Wire Line
	10175 4035 9775 4035
Connection ~ 9775 4035
Wire Wire Line
	10175 4485 10175 4835
Wire Wire Line
	9625 4685 10425 4685
Connection ~ 7525 4685
Wire Wire Line
	7525 5035 7525 5285
Connection ~ 8175 4985
Connection ~ 8175 5285
Wire Wire Line
	9625 4835 10425 4835
Connection ~ 10175 4835
Wire Wire Line
	7025 5985 10475 5985
Connection ~ 7025 5985
Wire Wire Line
	1000 1050 1400 1050
Wire Wire Line
	1000 1300 1400 1300
Wire Wire Line
	1000 1550 1400 1550
Wire Wire Line
	8400 1000 9200 1000
Wire Wire Line
	8400 1200 9200 1200
Wire Wire Line
	8400 1400 9200 1400
Wire Wire Line
	9740 1420 10540 1420
Wire Wire Line
	2550 1595 2550 1745
Wire Wire Line
	2350 1595 2550 1595
Wire Wire Line
	9745 980  10545 980 
Wire Wire Line
	9745 1180 10545 1180
Wire Wire Line
	7625 5585 7825 5585
Wire Wire Line
	7825 5585 7825 5285
Connection ~ 7825 5285
Wire Wire Line
	2750 1195 2350 1195
Wire Wire Line
	2750 1395 2350 1395
Wire Wire Line
	3215 5785 1600 5785
Wire Wire Line
	7525 4735 7525 4685
Wire Wire Line
	6470 1975 6770 1975
Wire Wire Line
	6470 2225 6770 2225
Wire Wire Line
	2285 5485 2640 5485
Wire Wire Line
	8095 4385 8525 4385
Wire Wire Line
	1600 4885 3215 4885
Wire Wire Line
	2195 5205 2195 4885
Connection ~ 2195 4885
Wire Wire Line
	2195 5505 2195 5785
Connection ~ 2195 5785
Wire Wire Line
	4525 5785 5945 5785
Wire Wire Line
	4515 5485 4720 5485
Wire Wire Line
	4505 5185 4715 5185
Wire Wire Line
	5015 5185 5240 5185
Wire Wire Line
	2750 1010 2350 1010
Wire Wire Line
	4505 4885 5930 4885
Wire Wire Line
	5020 5485 5250 5485
Wire Wire Line
	2940 5485 3215 5485
Wire Wire Line
	2940 5185 3215 5185
Wire Wire Line
	5495 4885 5495 5205
Wire Wire Line
	5495 5505 5495 5785
Connection ~ 5495 4885
Connection ~ 5495 5785
Text Notes 4055 6880 1    60   ~ 0
------------------------------------------\n\n\n------------------------------------------
Text Label 8095 4385 0    60   ~ 0
Hin
Text Label 8085 4535 0    60   ~ 0
Lin
Text Notes 6210 1780 0    60   ~ 0
DRIVER TEST POINTS
Text Notes 5985 1845 0    60   ~ 0
------------------
$Comp
L D_Schottky DD1
U 1 1 5AC6650E
P 9025 3985
F 0 "DD1" H 9025 4085 50  0000 C CNN
F 1 "MBR2H100SFT3G" H 9025 3885 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 9025 3985 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/1221/0900766b812212a8.pdf" H 9025 3985 50  0001 C CNN
F 4 "781-5650" H 9025 3985 60  0001 C CNN "Ref. RS"
	1    9025 3985
	-1   0    0    1   
$EndComp
Text Notes 7350 7500 0    60   ~ 0
DRIVER BLOCK
$EndSCHEMATC
