EESchema Schematic File Version 2
LIBS:Power_converter_V4-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:sensors
LIBS:Measurement_block-cache
LIBS:Measurement_block-rescue
LIBS:digital
LIBS:lm2672
LIBS:digital-cache
LIBS:feeder_block-cache
LIBS:Power_converter_V4-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 14
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 6900 4200 0    60   ~ 0
Vout
Wire Wire Line
	6450 4200 6900 4200
Wire Wire Line
	6050 3650 6050 3900
Wire Wire Line
	6050 4500 6050 5150
Text Label 5850 3650 2    60   ~ 0
+5VHigh
Text Notes 4500 2300 0    79   ~ 0
NON INVERTING BUFFER
Text Notes 4300 2650 0    59   ~ 0
This part senses the current from the low voltage side
Wire Notes Line
	4300 2400 6600 2400
Text HLabel 10250 1100 2    60   Output ~ 0
Vout
Wire Wire Line
	10250 1100 9850 1100
Text Label 9850 1100 0    60   ~ 0
Vout
Text Notes 10500 750  2    60   ~ 0
Output Variables
Wire Notes Line
	9550 1300 11000 1300
Wire Notes Line
	11000 1300 11000 1250
Text Notes 1100 800  0    60   ~ 0
Input Variables
Wire Notes Line
	4100 1850 4100 700 
Wire Notes Line
	750  1850 4100 1850
Text Label 2650 1550 2    60   ~ 0
GNDPwR
Text Label 2650 1350 2    60   ~ 0
+5VHigh
Text Label 1650 1550 2    60   ~ 0
VH-
Text Label 1650 1350 2    60   ~ 0
VH+
Wire Wire Line
	2250 1550 2650 1550
Text HLabel 2250 1550 0    60   Input ~ 0
GNDPwR
Wire Wire Line
	2250 1350 2650 1350
Text HLabel 2250 1350 0    60   Input ~ 0
+5VHigh
Wire Wire Line
	1250 1550 1650 1550
Wire Wire Line
	1250 1350 1650 1350
Text HLabel 1250 1550 0    60   Input ~ 0
VH-
Text HLabel 1250 1350 0    60   Input ~ 0
VH+
$Comp
L OP275 U1
U 1 1 595D6CCF
P 6150 4200
AR Path="/57067DAC/59547097/595D6CCF" Ref="U1"  Part="1" 
AR Path="/57067DAC/5955AA0A/595D6CCF" Ref="U2"  Part="1" 
AR Path="/57067DAC/5955B27F/595D6CCF" Ref="U1"  Part="2" 
AR Path="/57067DAC/5955DC6C/595D6CCF" Ref="U2"  Part="2" 
AR Path="/57067DAC/5955DC83/595D6CCF" Ref="U3"  Part="1" 
AR Path="/57067DAC/5955DC9A/595D6CCF" Ref="U3"  Part="2" 
AR Path="/595D6CCF" Ref="U1"  Part="2" 
AR Path="/59547097/595D6CCF" Ref="U1"  Part="1" 
AR Path="/5955AA0A/595D6CCF" Ref="U1"  Part="1" 
F 0 "U1" H 6150 4350 50  0000 L CNN
F 1 "OPA2336" H 6150 4050 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_Socket_LongPads" H 6050 4250 50  0001 C CNN
F 3 "" H 6150 4350 50  0000 C CNN
F 4 "Value" H 6150 4200 60  0000 C CNN "Bibliothèque/Application support/kicad/library/shield_arduino_Schematic"
F 5 "Value" H 6150 4200 60  0000 C CNN "Bibliothèque/Application support/kicad/library/SHIELD_ARDUINO_empreintes"
F 6 "Value" H 6150 4200 60  0000 C CNN "Bibliothèque/Application support/kicad/library/Shield_Arduino_3D"
F 7 "Value" H 6150 4200 60  0001 C CNN "Fieldname"
	1    6150 4200
	1    0    0    -1  
$EndComp
Wire Notes Line
	9550 550  9550 1300
Text Label 4750 4100 2    60   ~ 0
VH+
$Comp
L R Rp1
U 1 1 5970BA5A
P 5550 4500
AR Path="/57067DAC/59547097/5970BA5A" Ref="Rp1"  Part="1" 
AR Path="/57067DAC/5955AA0A/5970BA5A" Ref="Rp3"  Part="1" 
F 0 "Rp1" V 5630 4500 50  0000 C CNN
F 1 "R" V 5550 4500 50  0000 C CNN
F 2 "" V 5480 4500 50  0000 C CNN
F 3 "" H 5550 4500 50  0000 C CNN
	1    5550 4500
	1    0    0    -1  
$EndComp
$Comp
L R Rp2
U 1 1 5970BA80
P 6250 4550
AR Path="/57067DAC/59547097/5970BA80" Ref="Rp2"  Part="1" 
AR Path="/57067DAC/5955AA0A/5970BA80" Ref="Rp4"  Part="1" 
F 0 "Rp2" V 6330 4550 50  0000 C CNN
F 1 "R" V 6250 4550 50  0000 C CNN
F 2 "" V 6180 4550 50  0000 C CNN
F 3 "" H 6250 4550 50  0000 C CNN
	1    6250 4550
	0    1    1    0   
$EndComp
Wire Wire Line
	5550 4300 5850 4300
Wire Wire Line
	5550 4300 5550 4350
Wire Wire Line
	5200 4100 5850 4100
Wire Wire Line
	6050 3650 5850 3650
Wire Wire Line
	5750 4300 5750 4550
Wire Wire Line
	5750 4550 6100 4550
Connection ~ 5750 4300
Wire Wire Line
	6400 4550 6650 4550
Wire Wire Line
	6650 4550 6650 4200
Connection ~ 6650 4200
Wire Wire Line
	5550 5000 5550 4650
$Comp
L C CfiL1
U 1 1 597439B7
P 5250 4600
AR Path="/57067DAC/59547097/597439B7" Ref="CfiL1"  Part="1" 
AR Path="/57067DAC/5955AA0A/597439B7" Ref="CfiH1"  Part="1" 
F 0 "CfiL1" V 5300 4650 50  0000 L CNN
F 1 "1uF" V 5300 4400 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 5288 4450 50  0001 C CNN
F 3 "" H 5250 4600 50  0000 C CNN
	1    5250 4600
	-1   0    0    1   
$EndComp
$Comp
L R RfiL1
U 1 1 597439BE
P 5050 4100
AR Path="/57067DAC/59547097/597439BE" Ref="RfiL1"  Part="1" 
AR Path="/57067DAC/5955AA0A/597439BE" Ref="RfiH1"  Part="1" 
F 0 "RfiL1" V 5130 4100 50  0000 C CNN
F 1 "100" V 5050 4100 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4980 4100 50  0001 C CNN
F 3 "" H 5050 4100 50  0000 C CNN
	1    5050 4100
	0    1    1    0   
$EndComp
Wire Wire Line
	5250 4100 5250 4450
Connection ~ 5250 4100
Wire Wire Line
	4900 4100 4750 4100
Wire Wire Line
	5250 4750 5250 5000
Text Label 4750 5000 2    60   ~ 0
VH-
Text Label 6050 5150 2    60   ~ 0
GNDPwR
Wire Wire Line
	4750 5000 5550 5000
Connection ~ 5250 5000
$EndSCHEMATC
