EESchema Schematic File Version 2
LIBS:Power_converter_V4-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:sensors
LIBS:Measurement_block-cache
LIBS:Measurement_block-rescue
LIBS:digital
LIBS:lm2672
LIBS:digital-cache
LIBS:feeder_block-cache
LIBS:Power_converter_V4-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 14
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 6900 4200 0    60   ~ 0
Vout
Wire Wire Line
	6450 4200 6900 4200
Wire Wire Line
	6050 3450 6050 3900
Wire Wire Line
	5450 4600 5450 4850
Wire Wire Line
	6050 4850 6050 4500
Text Label 5750 3300 2    60   ~ 0
+5VHigh
Text Notes 4500 2300 0    79   ~ 0
BUFFER AMPLIFIER
Text Notes 4300 2650 0    59   ~ 0
This part XXXX
Wire Notes Line
	4300 2400 6600 2400
Text HLabel 10250 1100 2    60   Output ~ 0
Vout
Wire Wire Line
	10250 1100 9850 1100
Text Label 9850 1100 0    60   ~ 0
Vout
Text Notes 10500 750  2    60   ~ 0
Output Variables
Wire Notes Line
	9550 1300 11000 1300
Wire Notes Line
	11000 1300 11000 1250
Text Notes 1100 800  0    60   ~ 0
Input Variables
Wire Notes Line
	4100 1850 4100 700 
Wire Notes Line
	750  1850 4100 1850
Text Label 2650 1550 2    60   ~ 0
GNDPwR
Text Label 2650 1350 2    60   ~ 0
+5VHigh
Text Label 1650 1550 2    60   ~ 0
VH-
Text Label 1650 1350 2    60   ~ 0
VH+
Wire Wire Line
	2250 1550 2650 1550
Text HLabel 2250 1550 0    60   Input ~ 0
GNDPwR
Wire Wire Line
	2250 1350 2650 1350
Text HLabel 2250 1350 0    60   Input ~ 0
+5VHigh
Wire Wire Line
	1250 1550 1650 1550
Wire Wire Line
	1250 1350 1650 1350
Text HLabel 1250 1550 0    60   Input ~ 0
VH-
Text HLabel 1250 1350 0    60   Input ~ 0
VH+
$Comp
L OP275 U1
U 2 1 595D6CCF
P 6150 4200
AR Path="/57067DAC/59547097/595D6CCF" Ref="U1"  Part="1" 
AR Path="/57067DAC/5955AA0A/595D6CCF" Ref="U1"  Part="2" 
AR Path="/57067DAC/5955B27F/595D6CCF" Ref="U1"  Part="2" 
AR Path="/57067DAC/5955DC6C/595D6CCF" Ref="U2"  Part="2" 
AR Path="/57067DAC/5955DC83/595D6CCF" Ref="U3"  Part="1" 
AR Path="/57067DAC/5955DC9A/595D6CCF" Ref="U3"  Part="2" 
AR Path="/595D6CCF" Ref="U1"  Part="1" 
AR Path="/59547097/595D6CCF" Ref="U1"  Part="1" 
AR Path="/5955AA0A/595D6CCF" Ref="U1"  Part="1" 
F 0 "U3" H 6150 4350 50  0000 L CNN
F 1 "OPA2336" H 6150 4050 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_Socket_LongPads" H 6050 4250 50  0001 C CNN
F 3 "" H 6150 4350 50  0000 C CNN
F 4 "Value" H 6150 4200 60  0000 C CNN "Bibliothèque/Application support/kicad/library/shield_arduino_Schematic"
F 5 "Value" H 6150 4200 60  0000 C CNN "Bibliothèque/Application support/kicad/library/SHIELD_ARDUINO_empreintes"
F 6 "Value" H 6150 4200 60  0000 C CNN "Bibliothèque/Application support/kicad/library/Shield_Arduino_3D"
F 7 "Value" H 6150 4200 60  0001 C CNN "Fieldname"
	2    6150 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 4300 5750 4300
Wire Wire Line
	5750 4300 5750 4500
Wire Wire Line
	5750 4500 6600 4500
Wire Wire Line
	6600 4500 6600 4200
Connection ~ 6600 4200
Wire Wire Line
	5000 4100 5850 4100
$Comp
L D_ALT Dm1
U 1 1 596E0FF5
P 5450 3800
AR Path="/57067DAC/59547097/596E0FF5" Ref="Dm1"  Part="1" 
AR Path="/57067DAC/5955AA0A/596E0FF5" Ref="Dm1"  Part="1" 
AR Path="/57067DAC/5955B27F/596E0FF5" Ref="Dm1"  Part="1" 
AR Path="/59547097/596E0FF5" Ref="Dm1"  Part="1" 
AR Path="/5955AA0A/596E0FF5" Ref="Dm1"  Part="1" 
AR Path="/57067DAC/5955DC6C/596E0FF5" Ref="Dm3"  Part="1" 
AR Path="/57067DAC/5955DC83/596E0FF5" Ref="Dm5"  Part="1" 
AR Path="/57067DAC/5955DC9A/596E0FF5" Ref="Dm7"  Part="1" 
F 0 "Dm7" H 5450 3900 50  0000 C CNN
F 1 "Diode" H 5450 3700 50  0000 C CNN
F 2 "" H 5450 3800 50  0000 C CNN
F 3 "" H 5450 3800 50  0000 C CNN
	1    5450 3800
	0    1    1    0   
$EndComp
$Comp
L D_ALT Dm2
U 1 1 596E1024
P 5450 4450
AR Path="/57067DAC/59547097/596E1024" Ref="Dm2"  Part="1" 
AR Path="/57067DAC/5955AA0A/596E1024" Ref="Dm2"  Part="1" 
AR Path="/57067DAC/5955B27F/596E1024" Ref="Dm2"  Part="1" 
AR Path="/59547097/596E1024" Ref="Dm2"  Part="1" 
AR Path="/5955AA0A/596E1024" Ref="Dm2"  Part="1" 
AR Path="/57067DAC/5955DC6C/596E1024" Ref="Dm4"  Part="1" 
AR Path="/57067DAC/5955DC83/596E1024" Ref="Dm6"  Part="1" 
AR Path="/57067DAC/5955DC9A/596E1024" Ref="Dm8"  Part="1" 
F 0 "Dm8" H 5450 4550 50  0000 C CNN
F 1 "Diode" H 5450 4350 50  0000 C CNN
F 2 "" H 5450 4450 50  0000 C CNN
F 3 "" H 5450 4450 50  0000 C CNN
	1    5450 4450
	0    1    1    0   
$EndComp
Wire Wire Line
	5450 3950 5450 4300
Connection ~ 5450 4100
Wire Wire Line
	5450 3650 5450 3450
Wire Wire Line
	5750 3450 5750 3300
Connection ~ 5750 3450
Wire Wire Line
	5450 3450 6050 3450
Wire Notes Line
	9550 550  9550 1300
Text Label 5000 4100 2    60   ~ 0
VH+
Wire Wire Line
	5450 4850 6050 4850
Wire Wire Line
	5750 4850 5750 5200
Connection ~ 5750 4850
Text Label 5750 5200 2    60   ~ 0
GNDPwR
$EndSCHEMATC
